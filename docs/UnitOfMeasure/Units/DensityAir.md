### DensityAir

This unit represents the density of a substance in air.

#### Base unit

The internal value of DensityAir is Kg/m^3

#### Units

- Kg/m^3 *(default)* - 1 * basevalue
```
DensityAir.KilogramPerCubicMetre
UnitOfMeasureTypes.DensityKgm3Air
```
- t/m^3 - 1000 * basevalue
```
DensityAir.TonnePerCubicMetre
UnitOfMeasureTypes.Densitytm3Air
```
- Kg/L - 1000 * basevalue
```
DensityAir.KilogramPerLiter
UnitOfMeasureTypes.DensityKgLAir
```

#### Additional methods

###### Transform

Transform DensityVac to DensityAir
```
DensityAir air = DensityAir.Transform(vac);
```

#### Additional operators

###### DensityAir * VolumeGOV

Multiplying DensityAir with a unit VolumeGOV results in a MassAir

```
MassAir mass = density * volume
```
