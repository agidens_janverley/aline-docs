# Conversions

Not every quantity can be transformed into another type by a simple calculation.
Therefore we have created convertors. Convertors are designed to convert a quantity to another type of quantity.

### IConversion interface

All convertors inherit the same IConversion interface, hereby exposing the same methods.

### Common methods

By inheriting from IConversion the following methods are exposed from each convertor.
#### SourceType

Gets the type the convertor converts from. 

#### DestinationType

Gets the type of the result of the convertor.

#### Convert

This is were the magic happens.
Call this method with two parameters

- The Source value (to convert) which is an IQuantity 
- The parameters used for the conversion array of objects

The parameters are different for each convertor. They can be found in the manual or using the UnitProvider.
There Convert method is decorated with all the required parameters.

The result is an IQuantity and can be casted to the Destination type.

#### GetUsedParams

This method returns a dictionary<string,string> with the parameters used by the convertor.








