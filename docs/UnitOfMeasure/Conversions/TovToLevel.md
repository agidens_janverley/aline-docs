## TOV to Level conversion

This convertor converts a Volume of type TOV to the corresponding level of a strapping table.

### Init

To load the convertor, create a new instance of the TovToLevel class. Namespace: *Agidens.Terminal.Suite.Shared.UnitOfMeasure.Conversions*

```
var convertor = new TovToLevel();
```

#### SourceType

The source type of the convertor is **VolumeTOV**

#### DestinationType

The destination type is **Length**

### Conversion parameters

This convertor has 4 parameters

- The *Length* value of the strapping measurement above the TOV value
- The *Length* value of the strapping measurement below the TOV value
- The *VolumeTOV* value of the strapping above the TOV value
- The *VolumeTOV* value of the strapping below the TOV value

As you see, before calling the convertor you have to lookup the row in the strapping table above the value to convert and the row below.
If there is no value below you should set the value to 0. 
If value is an exact match of a strapping entry, both, below and above should be set to the same value.
