[Home](Home)

# `IResult`-validations

Validations are split according to functional blocks. Each validation has a unique number associated with it.

## Administration
1000-range.

### Product Group
- 1001 - NotEnabled

### Product
- 1021 - NotEnabled

### Product Synonym
- 1041 - NotEnabled

## Orders
### Planning
#### Sales Order
2000-range.

#### Sales Order Entry
2100-range.

### Execution
#### Work Order

- 2201 - StateChangeNotAllowed
- 2202 - IncorrectState
- 2203 - WorkOrderEntriesMissing
- 2204 - RequirementsNotValidated
- 2205 - WorkOrderEntryNotFound
- 2206 - OnlySingleWorkOrderEntryCanBeInStateReadyForTransfer
- 2207 - NoWorkOrderEntryCanBeReleased
- 2208 - WorkOrderEntriesInProgress

#### Work Order Entry

- 2301 - StateChangeNotAllowed
- 2302 - IncorrectState
- 2303 - NotEnoughProductAvailable
- 2304 - CompartmentOrderEntriesMissing
- 2305 - TransfersMissing
- 2306 - NotEnoughFreeSpaceIsAvailable

#### Compartment Order Entry
2400-range.

#### Transfer

- 2501 - EntryWeighingMissing
- 2502 - StartMeasurementMissing
- 2503 - OtherWeighingBasedWOEInProgress
- 2504 - OtherWeighingBasedWOEMissingExitWeighing

## Module: Access Control
3000-range.

## Module: Visits
4000-range.

## Module: Inventory
5000-range.

## Module: Contract Management

- 6001 - ModuleNotEnabled
- 6002 - NoAgreementsFound
- 6003 - InvalidHandlingType
- 6004 - MoreThanVolumeAssigned

## Module: Timesheets
7000-range.