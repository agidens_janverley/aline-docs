[Home](../Home)

#Technical overview
![2.png](../Resources/Reporting/TechnicalOverview.png)

The Operation Core executable can be configured to run the PrintServer service

`Egemin.Evita.OperationCore.Configuration.xml`

should contain a FunctionalUnit section describing all parameters:

```
  <functionalUnit name="PrintServer" exportTypeName="PrintingHandler" relativeModulePath=""
					xmlns="http://www.egemin.com/Evita/Egemin.Evita.Configuration.xsd"> 
    <SiteSpecificParameters>
      <string name="RabbitMqServer">host=localhost;username=aline;password=aline</string>
      <string name="SubscribeDocumentPrintRequestMsg">Nynas.Zone1.PrintServer.DocumentPrintRequest</string> 
      <string name="PrintFolder">C:\Agidens\Documents\</string> <!--Output path for saving reporst as pdf files--> 
      <string name="ReportsPath">C:\Agidens\Reports\</string> <!--Currently not used: backup location for looking for report .trdx files-->
      <string name="ReportsStoragePath">C:\Agidens\ReportsStorage\</string> <!-- Storage location for Telerik internal files-->
      <string name="ReportsHostBaseAddress">http://localhost:8080</string> <!--For initializing the Telerik Reporting Server (HttpSelfHostConfiguration)-->
      <string name="LogoPath">C:\projects\OperationCore-out\Logo\photo.jpg</string>
      <string name="DefaultLanguageIso2Code">en</string> <!--Fallback if no language is set in the PrintRequestMsg-->
    </SiteSpecificParameters>
    <Parameters>
      <wcfserviceaddress>net.tcp://localhost:8755/OperationCoreService/</wcfserviceaddress>
      <logFilePrefix>PS_</logFilePrefix>
      <logFolder>C:\Agidens\Logging\OperationCore\</logFolder>
      <traceGeneratorInformationLevel>Trace</traceGeneratorInformationLevel>
    </Parameters>
  </functionalUnit>
```

The config file for the Operation Core service itself, `E5-OCS.exe.config`, should contain this section.  
It tells Telerik to reference and use the code in these assemblies.  
They contain e.g. Types and Helper methods  for translations and displaying icons.

```
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="uaClientConfiguration" type="Opc.Ua.ApplicationConfigurationSection,Opc.Ua.Core" />
    <section name="Telerik.Reporting" type="Telerik.Reporting.Configuration.ReportingConfigurationSection, Telerik.Reporting, Version=11.1.17.614, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" />
  </configSections>
  <Telerik.Reporting>
    <assemblyReferences>
      <add name="Agidens.Terminal.Suite.Service.OrderManager.Dto"/>
      <add name="Agidens.Aline.OperationCore.Services.Printing"/>
      <add name="Agidens.Aline.OperationCore.Services.Printing.DefaultReports" />
    </assemblyReferences>
  </Telerik.Reporting>
  <system.serviceModel>
...
```

# Adjusting Reports

As described above, the "looks" of a reports is stored in an *.trdx file.
This is a basic xml file that you _could_ edit with Notepad.

##Necessary Tools 

However, there is a Telerik Tool that gives you a better experience editing these files.:

The Telerik Report Designer R2 2017  

![1.png](../Resources/Reporting/ReportDesigner.png)

### Installation
The installer is here: 
[Telerik_Reporting_R2_2017_SP1_11_1_17_614_DEV.msi]  

`\\nas-opn\Temp-ProjBackup\PX108_D0006 - MES-Aline\ALINE_THIRD_PARTY\Telerik\Telerik_Reporting_R2_2017_SP1_11_1_17_614_DEV.msi`

##Report data 
The Operation Core Printing Service retrieves the data related to the report, and passes it to the report instance.
So inside the `*.trdx` report definition file, you have no control over what data is retrieved from the Aline service.

## Modifying report files
The trdx files must be placed next to the executable that runs the OperationCore Service:

![3.png](../Resources/Reporting/ReportFiles.png)

You can directly edit the trdx file  
**No restart of the PrintService necessary.**  
If you have a preview window open in the Portal, you can just click Refresh to see the updated result.  

## Using Aline specific Functions and Types in the Designer
The designer is able to understand and use types that Aline created.

[More info here] (https://www.telerik.com/support/kb/reporting/details/how-to-use-external-assemblies-with-custom-user-functions-in-the-report-designer)

![1.png](../Resources/Reporting/AlineFunctionsDesigner.png)

## Adding Reports
**It is possible to add new (Project) reports.**  

### Kiosk
The kiosk can be configured to send any `SubscribeDocumentPrintRequestMsg` you want using existing config posibilities. 

### Portal

This screenshot was created without changing Aline product code:  

![4.png](../Resources/Reporting/PortalPrintPreview.png)

A few building blocks have been prepared to enable you to add reports.  

- The technical components (Winforms controls) needed to show the preview of the reports, have been placed together in a nuget package.   
This nuget package is build in this TeamCity config:
[Portal Master](http://10.10.14.20/viewType.html?buildTypeId=Aline_OrderManagerMaster&branch_Aline_OrderManager=%3Cdefault%3E&tab=buildTypeStatusDiv)

- Using this package, you can create your own project-specific assembly.  
- You can add Menu items in the Portal using the default epia infrastructure.  

![7.png](../Resources/Reporting/ProfileCatalog.png)

**You'll have to restart Portal Server and Shell, to pick up the change in ProfileCatalog.xml and the translations**  

An example is uploaded here:  
[TestReport.zip](../Resources/Reporting/TestReports.zip)

This example contains 2 projects:
- a module that can be added to the Portal 
- an assembly that can extend the PrintServer functionality 

### Extending the PrintServer (OperationCore service)

The Portal can display extra reports, but the print server has to deliver them.  
The PrintServer can also be extended using MEF.  

- Create a new assembly containing the logic to create your own reports based on the standard `DocumentPrintRequestMsg`

- Export an implementation of `IReportFactory'

![6.png](../Resources/Reporting/TestReportsPrintServer.png)

- Place the assembly next to the OperationCore Service executable, together with all *.trdx files

![5.png](../Resources/Reporting/TestReportFiles.png)

- Translations in the report are stored in the database (`Translation`, `TranslationResource` tables)

**You'll have to restart PrintServer, to pick up the new assembly and refresh the cached translations from the database**