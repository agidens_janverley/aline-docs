### Length

This unit represents length.

#### Base unit

The internal value of Length is m (metre)

#### Units

- m *(default)* - 1 * basevalue
```
Length.Metre
UnitOfMeasureTypes.m
```
- dm - 0.1 * basevalue
```
Length.Decimetre
UnitOfMeasureTypes.dm
```
- cm - 0.01 * basevalue
```
Length.Centimetre
UnitOfMeasureTypes.cm
```
- mm - 0.001 * basevalue
```
Length.Millimetre
UnitOfMeasureTypes.mm
```
- km - 1000 * basevalue
```
Length.Kilometre
UnitOfMeasureTypes.km
```

#### Additional methods

none

#### Additional operators

none

