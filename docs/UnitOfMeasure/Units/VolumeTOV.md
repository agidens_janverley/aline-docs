### VolumeTOV

This unit represents Total Observed Volume.

#### Base unit

The internal value of VolumeTOV is Kg/m^3

#### Units

- m^3 *(default)* - 1 * basevalue
```
VolumeTOV.CubicMetre
UnitOfMeasureTypes.m3TOV
```
- dm^3 - 0.001 * basevalue
```
VolumeTOV.CubicDecimetre
UnitOfMeasureTypes.dm3TOV
```
- cm^3 - 0.000001 * basevalue
```
VolumeTOV.CubicCentimetre
UnitOfMeasureTypes.cm3TOV
```
- mm^3 - 0.000000001 * basevalue
```
VolumeTOV.CubicMillimetre
UnitOfMeasureTypes.mm3TOV
```
- L - 0.001 * basevalue
```
VolumeTOV.Litre
UnitOfMeasureTypes.LTOV
```


#### Additional methods

none

#### Additional operators

none
```
