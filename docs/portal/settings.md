[Home](home)

**AmbientTemperatureInDegreesCelsius**

Type: **Decimal**
Default value: **15.0**
From version: **1.2.0.0**

*Ambient temperature on site. This is used in GOV/GSV/TOV/... conversions.*


**LocationChangeRegisteredSubscriptionTopic**

Type: **String**
From version: **1.2.0.0**

*The topic to which work order location changed messages (`WorkOrderLocationChangeRegisteredMsg`) are sent. The topic should match the topic on which the access and location service posts work order location update messages.*


**LocationChangeRequestPublishTopic**

Type: **String**
From version: **1.2.0.0**

*The topic to which work order location change messages (`WorkOrderLocationChangeRequestMsg`) are sent. The topic should match the topic on which the access and location service is listening for location change request messages.*