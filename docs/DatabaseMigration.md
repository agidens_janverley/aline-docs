# Database Migration #

To make the database migrations easier, a migration table has been added to the database.
A powershell script has been created to make the migration operations fast and easy.

## Migration files ##
For each change you make to the database, you should create a migration file. This file is a plain SQL file located in the *MigrationsV2* folder.
The migration files should follow a basic structure. No worries, it's easy.

### Migration file structure ###
Let's begin with the transaction. Each migration should be wrapped in a transaction. This makes sure that a failure during script execution does not mess up the database.
The first line of your migration script should be **BEGIN TRANSACTION** and the script should end with **COMMIT** and **GO**
```
BEGIN TRANSACTION

USE [$(database)]
GO

-- Your migration actions go here

COMMIT
GO
```

As you can see in the example above the database name is provided with a parameter. The database name can be set using a command argument of the db migration script. That way the scripts do not need to be adjusted when using a database name other then MegaVitaDev.

### Migration file filename ###

The filename of a migration file also follows a structure. The filename is used to execute the migration scripts in the right order (this might be important depending on the changes).
The file name follows the pattern **yyyyMMdd_HHmm_description.sql**
The description should be a few words that describe the changes to the database. An example of a valid migration filename is *20171127_0927_WorkOrderIntegrationState.sql* 

This file adds an intergration state column to the workorder and was create the 27th of november 2017 at 27 past 9.

The filename (without extension) is used to identify the migration in the database. This means, once the migration is created the filename **may not** change.

## Migrations and the Aline service ##

A check has been added to the service startup sequence. The required migrations should be added to the **REQUIRED_MIGRATIONS** variable of the context.

*MegaVitaContext class*

```
public static readonly string[] REQUIRED_MIGRATIONS = new[] {"20171127_0927_WorkOrderIntegrationState"};
```
Add your migration identifier to this array of strings to be veirfied at startup.
If one of the defined migration is not present in the __Migrations table in the database a NotSupported exception will be thrown with the missing migration in the message.

## Migrations table ##
When the first migration is added to the database, a *__Migrations* table is added to the database.
The table contains all applied migrations and the timestamp of when they are applied.

## Migration Script ##

A powershell script (**AlineDB.ps1**) has been written to automate the migration process.

The exact syntax of the commands can be retrieved with the --? argument.
```
Aline database migration script V0.1

AlineDB.ps1 COMMAND [-filename FILENAME] -server SQLINSTANCE -username USERNAME -password PASSWORD -database DATABASE
   COMMAND: 
      MIGRATION [-filename ]FILENAME -server SQLINSTANCE -username USERNAME -password PASSWORD -database DATABASE
      Apply the migration FILENAME to the database

      UPGRADE -server SQLINSTANCE -username USERNAME -password PASSWORD
      Upgrade the database to the latest version

      CREATE -server SQLINSTANCE -username USERNAME -password PASSWORD
      Create the Aline database.
      If the database already exists it is dropped and recreated.

   FILENAME: The filename of a migration file 

   SERVER: The sql instance to execute on. (Default: .\ ) 

   USERNAME: The username that has to be used to connect to the database (Default: sa )

   PASSWORD: The password that has to be used to connect to the database (Default: )

   DATABASE: The database (Default: MegaVitaDev)
```

The script has three commands. MIGRATION, UPGRADE and CREATE.
### MIGRATION ###
Use the MIGRATION command to apply a specific migration file to the database.
The file provided with the -filename parameter will be applied to the database. If already present it will be omitted.

### UPGRADE ###
Use the UPGRADE command to upgrade the database to the latest version.
Upon execution of this command all migration files found in the MigrationsV2 folder will be checkend and if not applied yet, they will be applied to the database (and added to the migrations table).

### CREATE ###
With the CREATE command you can create a new database, or reset the current database.
The create command will execute all sql scripts in the current folder (being the 1 Datastore folder) and afterwards all migrations will be applied.

#### point of interest ####
The script is a powershell script. You should run it from powershell or use the powershell command to run it as powershell script.

The basic command to do this is from the commandline:
```
powershell -ExecutionPolicy Bypass -File AlineDB.ps1 COMMAND [-filename FILENAME] -server SQLINSTANCE -username USERNAME -password PASSWORD -database DATABASE 
```
Replace the placeholders with the real parameters before execution, or remove them if you want to use the default values (check the scripts help).

## More points of interest ##

### script changes ###
The basic scripts have been altered to be able to be executed by the AlineDB script. For example has the USE database been altered to a command with a variable that is set by the AlineDB script.

Hereby the scripts can **not be executed manually**.

When making changes to one of the basic scripts (normally you wouldn't you should create a migration) make sure you use GO commands when needed and use $(database) when you need the database name.

### Batch scripts for default actions ###

To spare time of the developer 2 batch scripts have been created with default parameters.
#### CreateDB.bat ####
Reset the database. The CREATE command will be executed for database *MegaVitaDev* on the *default SQL instance* of your local machine. The user used is *sa* with the *default agidens password*.
Basically the command execute is:
```
AlineDB.ps1 CREATE -username sa -password ******** -database MegaVitaDev -server .\
```
Password is hidden. This should not be in a document. If you don't know the password, you should possibly not be reading this document.

If you have your local SQL server on another instance or have other credentials set, please feel free to copy the batch file and change your settings. Just make sure the file is ignored by git and it becomes you local file.

Doing so you will be able to reset your database with one double click.

#### UpgradeDB.bat ####
This script deserves the same documentation as the script above. With the only difference that it executes the UPGRADE command instead of the CREATE.
If you have no idea, please start at the top and re-read the document. With attention this time.


