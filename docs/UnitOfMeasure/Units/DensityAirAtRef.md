### DensityAirAtRef

This unit represents the density of a substance in air at the reference temperature.

#### Base unit

The internal value of DensityAirAtRef is Kg/m^3

#### Units

- Kg/m^3 *(default)* - 1 * basevalue
```
DensityAirAtRef.KilogramPerCubicMetre
UnitOfMeasureTypes.DensityKgm3AirAtRef
```
- t/m^3 - 1000 * basevalue
```
DensityAirAtRef.TonnePerCubicMetre
UnitOfMeasureTypes.Densitytm3AirAtRef
```
- Kg/L - 1000 * basevalue
```
DensityAirAtRef.KilogramPerLiter
UnitOfMeasureTypes.DensityKgLAirAtRef
```

#### Additional methods

###### ConvertTo

Transform DensityVacAtRef to DensityAirAtRef
```
DensityAirAtRef air = DensityAirAtRef.ConvertTo(vac);
```

#### Additional operators

###### DensityAirAtRef * VolumeGSV

Multiplying DensityAirAtRef with a unit VolumeGSV results in a MassAir

```
MassAir mass = density * volume
```
