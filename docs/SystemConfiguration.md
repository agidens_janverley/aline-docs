# System Configuration #

The A-line application has a default behavior. This behavior can be altered by changing System options. To set an alternative value for a system option, you need to enter a new entry (or update the existing one) in the **SystemOptions** table.

The **Name** column holds the name (key) of the options. The **value** column the value. The value can be either a string, integer, boolean or decimal.

## Access control ##

**ACCESSCONTROL\_DISABLED**
Type: **Boolean**
Default value: **false (0)**
*Disable the access control. When set to true every accesscontrol check will be answered with Access granted.*

**ALLOWED\_IDENTIFICATION\_TYPES**
Type: **List<IdentificationTypes>**
Default value: **Badge** and **Pincode**
*Identification types that can be used by the system. Other identification types not defined in this setting will generate an not supported exception when used.*

**DEFAULTACCESSPROFILE\_DRIVER**
Type: **Integer**
Default value: **1**
*The default access profile for a driver. When a driver is automatically created (for example by a kiosk) a driver gets this accessprofile attached automatically.*

**DEFAULTACCESSPROFILE\_VISITOR**
Type: **Integer**
Default value: **2**
*The default access profile for a visitor. When a visit is created for a visitor and no accessprofile have been set on the visit or visitor, this profile is used by default.*

**DEFAULTACCESSPROFILE\_VISITORVEHICLE**
Type: **Integer**
Default value: **3**
*The default access profile for a visitor signing in with a vehicle. When a visitvehicle is created and the no accessprofile have been set on the visitvehicle, this profile is used by default. When attaching a visitor to the vehicle, the visitor will get this profile set by default.*

**IDENTIFICATIONCODE\_REGEX**
Type: **String**
Default value: **[a-z0-9]{5}**
*The format of the regex defines the token identifiers that are auto-generated.*

**RM\_PERSON\_IDENTIFICATION\_ON\_WO\_CANCEL**
Type: **Boolean**
Default value: **false (0)**
From version: **1.2.0.0**
*When a WO is cancelled, detach the identification from the person.*

**RM\_PERSON\_IDENTIFICATION\_ON\_WO\_CLOSE**
Type: **Boolean**
Default value: **false (0)**
*When a WO is closed, detach the identification from the person.*

**TOKEN\_LENGTH**
Type: **Dictionary<IdentificationTypes, int>**
Default value: **Pincode: 4**
*The exact length a token must have. This to ensure the proper processing when the token identifier is provided through hardware, instead of manual entry.*

**TOKEN\_NUMBER\_LENGTH**
Type: **Integer**
Default value: **6**
*The length of a token number received from hardware. Since token numbers have a specific length and the hardware controllers always send a fixed length string, this options should be set accordingly.*

**TOKEN\_NUMBER\_PREPEND\_CHAR**
Type: **String**
Default value: **0**
*This char is used as placeholder to fill up a number to meet the defined length in the length parameter. This should be the placeholder char the hardware controllers use.*

**TOKEN\_VALIDATION\_REGEX**
Type: **Hashtable<IdentificationTypes, String>**
Default value: **Pincode ^[0-9]{4}$** and **Badge ^[0-9]{1,6}$**

## Caching ##

**CACHE\_INVALIDATION\_TIMEOUT**
Type: **Integer**
Default value: **2**
*The number of minutes after which the data in the cache are invalidated and should be refreshed on access.*

## Calculation  ##

**VCF_NUMBER_OF_DECIMALS**
Type: **Integer**
Default Value: **5**
*When calculating the volume correction factor of a product, the factor is rounded. This parameter defines the number of decimals.*
Minimum value: 0, maximum value: 14

## Compartimentalisation ##

**BULKHEAD\_LOWER\_BOUNDARY**
Type: **Decimal** (Percentage)
Default value: **0**
*Defines the minimum boundary a compartment can be filled in case of an ADR product and the compartment has bulkheads.*

**BULKHEAD\_UPPER\_BOUNDARY**
Type: **Decimal** (Percentage)
Default value: **95**
*Defines the maximum boundary a compartment can be filled in case of an ADR product and the compartment has bulkheads.*

**CENTER\_TARGET\_QTY\_BETWEEN\_DEVIATION**
Type: **Boolean** 
Default value: **true (1)**
*On compartimentalisation the calculations aims to fill the container to the target quantity. When deviations are present, and this option is true, the target is calculated to be in the middle of the deviation. If false, the given target is used.*

**DEFAULT\_LOWER\_BOUNDARY**
Type: **Decimal** (percentage)
Default value: **20**
*Defines the minimum boundary a compartment can be filled in case of an ADR product.*

**DEFAULT\_UPPER\_BOUNDARY**
Type: **Decimal** (Percentage)
Default value: **95**
*Defines the maximum boundary a compartment can be filled in case of an ADR product.*

## Default values ##

**DEFAULT\_PRODUCT\_REFERENCE\_TEMPERATURE**
Type: **Decimal**
Default value: **15**
*The default reference temperature that will be set on creation or import of a new product when no reference temperature is provided.*

**DEFAULT\_WORKORDER\_TYPE**
Type: **WorkOrderType**
Default value: **Truck**
*The default type of workorder that is used on creation.*

## ERP ##

**SYSTEM\_HAS\_ACCESS\_TO\_ERP**
Type: **Boolean**
Default value: **false (0)**
*Indication if the customer uses an erp system to feed data to Aline.*

## Inventory ##

**INVENTORY\_CHOOSE\_TANK\_BASED\_ON\_LEVEL**
Type: **Boolean**
Default value: **false (0)**
*Indication if tank selection is based on its level. When set to **true**, a tank with a higher level is selected over other tanks that contain the same product and have a lower level.*

**INVENTORY\_MAXIMUM\_NUMBER\_OF\_ROWS**
Type: **Integer**
Default value: **1000**
*Maximum number of rows to return when retrieving historic stock-movement details.*

**INVENTORY\_SETPOINT\_INTERVAL**
Type: **Integer**
Default value: **7**
*Interval, in days, between each intermediate inventory 'save'-point. This to reduce calculations when retrieving current/historic stock for a tank.*

**INVENTORY\_SETPOINT\_NULLREF**
Type: **Datetime**
Default value: **August 1st 2017**
*Start date for inventory calculations.*

## LoadingBay ##

**LEVEL2\_NEXTCOMPARTMENT\_OVERRIDE\_ALLOWED**
Type: **Boolean**
Default value: **false (0)**
*On execution, A-Line decides the next compartment to execute. When this option is set to true, level2 may request the start of another compartment. If false, level2 should start the compartment A-Line sets next to be executed.*

**ADD\_ENTRYWEIGHING\_ON\_SETREADYFORTRANSFER**
Type: **Boolean**
Default value: **false (0)**
*On setting a workorder for a truck to ready for transfer, the entryweighing of the workorder is automatically added to the workorder entry as entryweighing. When this switch is on, only one woe can be in the set ready for transfer state at one. This switch also acticates some logic that starts and stops transfers for the woe in ready for transfer when a new weighing is added to the workorder. (This weighing will become the exit weighing for the woe)*

## Means of transport ##

**MOT\_SANITATION\_TOKENS**
Type: **String**
Default value: **- (dash and space)**
*The characters in this string are filtered from the input of an identification for a means of transport (license plate, container number, ...). With the default setting, AN-GB-55 will be handled as ANGB55 in the database and processing.*

## Modules

**CONTRACTMANAGEMENT**
Type: **Boolean**
Default Value: **false (0)**
*This setting determines if the module Contract Management is enabled. Compliance of WorkOrders is only checked if the module is enabled.
Licensing, pricing, deployment requirements and considerations are beyond the scope of this simple Boolean.*

## Reporting ##

**QUALITY\_LOGGED_DENSITY\_AT\_REF\_TEMP**
Type: **Boolean**
Default value: **true (1)**
*Indicator of the temperature used for the density logged in the quality results. If the option is true, the densitity is at reference temperature, if false, the density is at current temperature.*

## Tank ##

**TANK\_DEFAULT\_LOWER\_BOUNDARY**
Type: **Decimal**
Default value: **0**
*Lower boundary to which a tank can be filled as a percentage of the complete capacity.*

**TANK\_DEFAULT\_UPPER\_BOUNDARY**
Type: **Decimal**
Default value: **95**
*Upper boundary to which a tank can be filled as a percentage of the complete capacity.*

## Tracing ##

**TRACING\_MIN\_LEVEL**
Type: **Integer**
Default value: **1 (Error)**
*Indicates what the tracing level is when no level is set in the config.*
Allowed values include: 1: Error, 2: Warning, 3: Info, 4: Verbose.

## Transfers ##

**WEIGHING\_HANDLINGTYPE\_ENDPOINTS**
Type: **List<HandlingTypeEndpoints\>**
Default value: **Truck** & **RTC**
*Handling types contain two endpoints. In TankToTruck would these be tank and truck. This setting indicates for which type of endpoints weighings are required.*
Allowed values include: Undefined, Tank, Truck, Ship, Barge, RTC.
Multiple values should be stored in the database as 1 concatenated string separated by a pipe. "Truck|RTC" would result in a list of 2 items containing Truck and RTC.

**MEASUREMENT\_HANDLINGTYPE\_ENDPOINTS**
Type: **List<HandlingTypeEndpoints\>**
Default value: **Tank**
*Indicates for which handling types measurements are required.*
Allowed values include: Undefined, Tank, Truck, Ship, Barge, RTC.
Multiple values should be stored in the database as 1 concatenated string separated by a pipe. "Truck|RTC" would result in a list of 2 items containing Truck and RTC.

**COPY\_COMPARTMENTS**
Type: **List<MeansOfTransportTypes>**
Default value: **Barge** & **Ship**
*A copy of the compartment will automatically be created as a work order compartment when attaching a salaes order entry to a work order entry*

**RECALCULATETRANSFERES\_USE\_QUANTITY\_TRANSFERRED**
Type: **Boolean**
Default value: **True**
*Recalculate the transfers when an exit weighin is applied by using the ratio of the transferred quantity on transfer. If this is false the planned quantities are used for recalculation*

##WorkOrderEntry##
**VALIDATE\_LOADING\_RANGES**
Type: **Boolean**
Default value: **True**
*If true, when validating the work order entry the loading ranges will be validated for loading handling types.*