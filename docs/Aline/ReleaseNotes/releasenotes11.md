[Home](home)

# Release Notes Aline 1.1 #

Source: Youtrack Aline Fix versions: 1.1.0.0

##Bug


- [AL-661](http://10.10.14.20:8081/issue/AL-661) - Tank Strapping - Error when importing from XLS;
- [AL-763](http://10.10.14.20:8081/issue/AL-763) - Error on staff member delete;
- [AL-778](http://10.10.14.20:8081/issue/AL-778) - Validation step of WOE doesn't take loading boundaries into account;
- [AL-895](http://10.10.14.20:8081/issue/AL-895) - Edit product - Checkbox "Enable" is disabled;
- [AL-934](http://10.10.14.20:8081/issue/AL-934) - TOV-131: "lijnen ophouden";
- [AL-1042](http://10.10.14.20:8081/issue/AL-1042) - Operation Instruction - Loading area is missing on the report;
- [AL-1043](http://10.10.14.20:8081/issue/AL-1043) - Planning - Source and destination equipment;
- [AL-1076](http://10.10.14.20:8081/issue/AL-1076) - Unhandled exception when saving visit;
- [AL-1081](http://10.10.14.20:8081/issue/AL-1081) - ERP Interface overwrites its own files when same entity is imported;
- [AL-1102](http://10.10.14.20:8081/issue/AL-1102) - Loading Instruction -Destination;
- [AL-1103](http://10.10.14.20:8081/issue/AL-1103) - Compartimentalization uses Product instead of Content density;
- [AL-1105](http://10.10.14.20:8081/issue/AL-1105) - Stocklist Stock Changes not correctly shown on report;
- [AL-1107](http://10.10.14.20:8081/issue/AL-1107) - StockList report - Tanks without movements are not included in the report;
- [AL-1111](http://10.10.14.20:8081/issue/AL-1111) - Attaching a badge to a visitor;
- [AL-1112](http://10.10.14.20:8081/issue/AL-1112) - Content QP's are not used when no deviation;
- [AL-1113](http://10.10.14.20:8081/issue/AL-1113) - Content QP's are not used when calculating planned transfer volume;
- [AL-1115](http://10.10.14.20:8081/issue/AL-1115) - Kiosk behavior is interrupted when cancelling flow on the end message;
- [AL-1131](http://10.10.14.20:8081/issue/AL-1131) - Tank: Free space incorrect.;
- [AL-1132](http://10.10.14.20:8081/issue/AL-1132) - Transfer to tank must take free space into account;
- [AL-1134](http://10.10.14.20:8081/issue/AL-1134) - Stock list entry totals on wrong line;
- [AL-1140](http://10.10.14.20:8081/issue/AL-1140) - Unable to specify the MIN / MAX loading boundary for a Tank from the portal;
- [AL-1141](http://10.10.14.20:8081/issue/AL-1141) - Current release of the operation core implements old version of the evita Uasdk;
- [AL-1145](http://10.10.14.20:8081/issue/AL-1145) - Printing Bill of Lading directly from Portal results in empty page;
- [AL-1151](http://10.10.14.20:8081/issue/AL-1151) - QTY COE planned to be executed is wrong;
- [AL-1154](http://10.10.14.20:8081/issue/AL-1154) - Inventory Density uses logged temp instead of ref temp of product;
- [AL-1156](http://10.10.14.20:8081/issue/AL-1156) - OPC Client fails on AttributeName allready in collection;

##Enhancement

- [AL-902](http://10.10.14.20:8081/issue/AL-902) - Report Stocklist - entries grouped by WOE instead of TR;
- [AL-942](http://10.10.14.20:8081/issue/AL-942) - Manual inventory booking - Non-truck;
- [AL-1079](http://10.10.14.20:8081/issue/AL-1079) - Add a pincode to a visitor from the Visit screen;

## Epic

- [AL-782](http://10.10.14.20:8081/issue/AL-782) - Preparation Kiosk @ Preparation - Unloading - @ Dispatch;
- [AL-800](http://10.10.14.20:8081/issue/AL-800) - Drop Container;

## User Story

- [AL-759](http://10.10.14.20:8081/issue/AL-759) - Update the destination location of a Work Order when updating the state of Work Order Entry;
- [AL-795](http://10.10.14.20:8081/issue/AL-795) - Unloading compartmentalize screen;
- [AL-960](http://10.10.14.20:8081/issue/AL-960) - Configuration of the service to close visits;