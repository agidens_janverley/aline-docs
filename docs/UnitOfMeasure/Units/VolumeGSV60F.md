### VolumeGSV60F

This unit represents Gross Standard Volume at 60 �F.

#### Base unit

The internal value of VolumeGSV60F is Kg/m^3

#### Units

- m^3 *(default)* - 1 * basevalue
```
VolumeGSV60F.CubicMetre
UnitOfMeasureTypes.m3GSV60F
```
- gal (US) - 0.003785411784 * basevalue
```
VolumeGSV60F.GallonUS
UnitOfMeasureTypes.GalUSGSV60F
```
- gal (imp) - 0.00454609 * basevalue
```
VolumeGSV60F.GallonImp
UnitOfMeasureTypes.galimpGSV60F
```
- Barrel - 0.158987294928 * basevalue
```
VolumeGSV60F.BarrelBbl
UnitOfMeasureTypes.bblGSV60F
```

#### Additional methods

none

#### Additional operators

###### VolumeGSV60F * DensityAir

Multiplying VolumeGSV60F with a unit DensityAir results in a MassAir

```
MassAir mass = volume * density
```

###### VolumeGSV60F * DensityVac

Multiplying VolumeGSV60F with a unit DensityVac results in a MassVac

```
MassVac mass = volume * density
```
