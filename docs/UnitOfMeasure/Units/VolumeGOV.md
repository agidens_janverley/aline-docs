### VolumeGOV

This unit represents Gross Observed Volume.

#### Base unit

The internal value of VolumeGOV is Kg/m^3

#### Units

- m^3 *(default)* - 1 * basevalue
```
VolumeGOV.CubicMetre
UnitOfMeasureTypes.m3GOV
```
- dm^3 - 0.001 * basevalue
```
VolumeGOV.CubicDecimetre
UnitOfMeasureTypes.dm3GOV
```
- cm^3 - 0.000001 * basevalue
```
VolumeGOV.CubicCentimetre
UnitOfMeasureTypes.cm3GOV
```
- mm^3 - 0.000000001 * basevalue
```
VolumeGOV.CubicMillimetre
UnitOfMeasureTypes.mm3GOV
```
- L - 0.001 * basevalue
```
VolumeGOV.Litre
UnitOfMeasureTypes.LGOV
```
- gal (US) - 0.003785411784 * basevalue
```
VolumeGOV.GallonUS
UnitOfMeasureTypes.GalUSGOV
```
- gal (imp) - 0.00454609 * basevalue
```
VolumeGOV.GallonImp
UnitOfMeasureTypes.galimpGOV
```
- Barrel - 0.158987294928 * basevalue
```
VolumeGOV.BarrelBbl
UnitOfMeasureTypes.bblGOV
```

#### Additional methods

none

#### Additional operators

###### VolumeGOV * DensityAir

Multiplying VolumeGOV with a unit DensityAir results in a MassAir

```
MassAir mass = volume * density
```

###### VolumeGOV * DensityVac

Multiplying VolumeGOV with a unit DensityVac results in a MassVac

```
MassVac mass = volume * density
```
