# Kiosk Flow Configuration #

## AdrDriverValidationFlow ##
### AdrCertificateValidityInYears ###
Type: **Integer**
Default value: **5**
Dependencies: **CanCreateAdrForDriverOnAdrDriverValidation**

Used to initialise the keyboard for entering the adr certificate's end date. The year-range for the entry of the certificate's end date will go from *(Current Year)* up to *(Current Year + AdrCertificateValidityInYears)*, where both ends of the range are inclusive.

### CanCreateAdrForDriverOnAdrDriverValidation ###
Type: **Boolean**
Default value: ***false***

Indication if an ADR-certificate can be created if no valid ADR-certificate was found for the driver who is being validated.

## BadgeCollectorFlow ##
### BadgeCollectorSubscriberTopic ###
Type: **String**
Default value: ***null***

The topic that is being subscribed to, to listen for *BadgeSwallowedMsg*-, *BadgeRejectedMsg*-, *BadgeEjectedMsg*- and *BadgeAutoCloseWorkOrderFailed*-messages that indicate what action the badge collector performed.

### BadgeEjectionMessageTimer ###
Type: **Nullable Integer**
Default value: ***null***

Time, in seconds, that the message is displayed on screen, before the flow continues.

### CollectorImage ###
Type: **String**
Default value: ***null***

Name of an image that is located in the *'Resources'*-folder. It shows the image if found, otherwise the regular icon is shown.

## BadgeIdentificationFlow ##
### BadgeIdentificationImage ###
Type: **String**
Default value: ***null***

Name of an image that is located in the *'Resources'*-folder. It shows the image if found, otherwise the regular icon is shown.

### BadgeIdentificationTopic ###
Type: **String**
Default value: ***null***

### BadgeIdentificationUseNumericKeyboardOnly ###
Type: **Boolean**
Default value: ***false***

Indication if the configuration of the numeric keyboard (*NumericKeyBoardConfiguration*) is to be used, or the regular keyboard (*KeyboardConfiguration*), as configured by the application.

### CheckPersonAttached ###
Type: **Boolean**
Default value: ***false***

Indication if a verification step needs to be performed to make sure the identification is attached to a person.

### CheckWorkOrderAttached ###
Type: **Boolean**
Default value: ***false***

Indication if a verification step needs to be performed to make sure the identification is attached to a work order.

### ConnectedAccessControlDevice ###
Type: **String**
Default value: ***null***

Name of the badge reader to use to verify if the user has access.

## BarrierFlow ##
### Barrier_PublishTopic ###
Type: **String**
Default value: ***null***

The topic that is being used to publish *BarrierMsg*-messages to, to control the barrier.

### BarrierName ###
Type: **String**
Default value: ***null***

Name of the barrier that is controlled by the kiosk.

### BarrierStateToSend ###
Type: **BarrierStates**
Default value: **BarrierStates.Pulse**
Possible values: **Closed**, **Open**, **Pulse**

Barrier state to send to the barrier.

## CallOffFlow ##
Initially, work orders that have a state that is contained within *CallOffWorkOrderStatesOfInterest* or have a work order entry that has a state contained in *CallOffWorkOrderEntryStatesOfInterest* are retrieved from the database and are shown on the cal-off screen. After this initialisation phase, the call-off flow listens to RabbitMq messages and periodically retrieves data from the database about work orders that have changed location.

### CallOffAutoRefreshTimeInSeconds ###
Type: **Nullable Integer**
Default value: ***null***

The time, in seconds, before an updated list of work orders is retrieved from the database. If this setting is missing or its value is 0, no periodic retrievals from the database will happen.

### CallOffBackgroundColour ###
Type: **String**
Default value: ***null***

The background colour of the call-off screen in the format '*#RRGGBB*' or '*#AARRGGBB*'.

### CallOffDefaultItemColour ###
Type: **String**
Default value: ***null***

The colour for a work order being displayed in the format '*#RRGGBB*' or '*#AARRGGBB*', if not overruled by *CallOffLocationsOfInterest*.

### CallOffFocusTimeInSeconds ###
Type: **Nullable Integer**
Default value: ***null***

The time, in seconds, when an item that is currently not displayed on the call-off screen will come into focus at the center of the screen. If this setting is missing or its value is 0, new items will just appear on the call-off screen without any special focus.

### CallOffItemFocusIntervalInMilliseconds ###
Type: **Integer**
Default value: ***null***

The time, in milliseconds, in between items that are being focused as described by *CallOffFocusTimeInSeconds*.

### CallOffItemFormat ###
Type: **String**
Default value: ***null***

Format of the items being displayed on the call-off screen. The following tags are supported:
* [DriverIdentification] *The identifier of the driver's active identification.*
* [DriverName] *The full name of the driver.*
* [LicensePlate] *The license plate of the first truck or tractor of the driver's means of transport combination.*
* [WorkOrderIdentification] *The identifier of identification linked to the driver's work order.*

Example:

* Driver: John Smith with a tractor (1-abc 123) / trailer (2-cde 456)
* Format: '`[DriverName] ([LicensePlate])`'
* Result: '`John Smith (1-abc 123)`'

### CallOffLocationsOfInterest  ###
Type: **String**
Default value: ***null***

List of locations the work orders need to be in, in order for them to be displayed on the call-off screen. Multiple locations can be specified and need to be separated by a pipe-character ('|'). Each location can have a colour appended. This colour will be used to display its items in. The colour can be in one of 2 formats: '*#RRGGBB*' or '*#AARRGGBB*'.

Example:

* Setting: '`Location 1|Location 2#FF0000`'
* Result: Work orders need to be in either location '*Location 1*' or location '*Location 2*' to be displayed. Work orders for location '*Location 1*' will be displayed using the colour *CallOffDefaultItemColour*. Work orders for location '*Location 2*' will be displayed using the colour '*#FF0000*'.

### CallOffNumberOfColumns ###
Type: **Integer**
Default value: ***null***

Items on the call-off screen are displayed using a grid. This setting specifies the number of columns of the grid.

### CallOffNumberOfRows ###
Type: **Integer**
Default value: ***null***

Items on the call-off screen are displayed using a grid. This setting specifies the number of rows of the grid.

### CallOffWorkOrderEntryStatesOfInterest ###
Type: **List<WorkOrderEntryStates\>**
Default value: **Empty list**

List of states a work order entry of a work order needs to be in, in order for the work order to be displayed on the call-off screen. Multiple states can be specified and need to be separated by a pipe-character ('|').

Example:

* Setting: '`Open|InProgress`'
* Result: Work orders need to have at least 1 work order entry in state '*Open*' or '*InProgress*' in order for them to be displayed. If this setting is used in combination with setting *CallOffWorkOrderStatesOfInterest*,the work order the work order entry belongs to also needs to be in a state defined in *CallOffWorkOrderStatesOfInterest* in order for the work order to be displayed on the call-off screen.

### CallOffWorkOrderStatesOfInterest ###
Type: **List<WorkOrderStates\>**
Default value: **Empty list**

List of states a work order needs to be in, in order for the work order to be displayed on the call-off screen. Multiple states can be specified and need to be separated by a pipe-character ('|').

Example:

* Setting: '`Open|InProgress`'
* Result: Work orders need to be in state '*Open*' or '*InProgress*' in order for them to be displayed. If this setting is used in combination with setting *CallOffWorkOrderEntryStatesOfInterest*, a work order also needs to have at least 1 work order entry in a state defined in *CallOffWorkOrderEntryStatesOfInterest* in order for the work order to be displayed on the call-off screen.

### WorkOrderLocationChangeRegisteredTopic ###
Type: **String**
Default value: ***null***

The topic that is being subscribed to, to listen for *WorkOrderLocationChangeRegisteredMsg*-messages that indicate work order location changes.

## CheckLoadingDirectionFlow ##
*No configurable settings.*

## CheckOrderExistsFlow ##
*No configurable settings.*

## CheckTractorExistsFlow ##
Flow to quickly check if a driver should get access to the terminal by validating the truck-part of his complete means of transport combination.

### CanCreateNonExistingWorkOrderMeansOfTransport ###
Type: **Boolean**
Default value: ***false***

Indication if a truck can be created if it isn't found in the datastore.

## CheckWoLocationFlow ##
Validates the current location of a work order against a list of locations.

### CheckWoLocation_ErrorMessageResourceKey ###
Type: **String**
Default value: ***null***

Resource name of the error message to display when the work order is not in a correct location.

### CheckWoLocation_LocationName ###
Type: **List<String\>**
Default value: **Empty list**

List of locations the work orders need to be in. Multiple locations can be specified and need to be separated by a pipe-character ('|').

## CreatePincodeFlow ##
*No configurable settings.*

## CreateWorkOrderFlow ##
*No configurable settings.*

## DischargingFlow ##
### CanAddAndResolveTransfers ###
Type: **Boolean**
Default value: ***false***

Indication if transfers will be created and added to the generated compartment order entries.

### CanReplaceDestinationEquipment ###
Type: **Boolean**
Default value: ***false***
Dependencies: **ResolveEquipment**

Indication if the equipment resolver can replace the destination equipment(s) of a work order entry. This setting has no effect when *ResolveEquipment* is set to *false*.

### CanReplaceSourceEquipment ###
Type: **Boolean**
Default value: ***false***
Dependencies: **ResolveEquipment**

Indication if the equipment resolver can replace the source equipment(s) of a work order entry. This setting has no effect when *ResolveEquipment* is set to *false*.

### DischargingConfirmMessageRequired ###
Type: **Boolean**
Default value: ***true***
Dependencies: **ManuallyCompartimentalize**

If *ManuallyCompartimentalize* is set to *true*, a message will be displayed to the driver to go to dispatch. This setting is an indication if the message needs to be confirmed by the driver, or that it automatically disappears after *DischargingMessageTimeoutInSeconds* seconds.

### DischargingMessageImage ###
Type: **String**
Default value: ***null***

Name of an image that is located in the *'Resources'*-folder. It shows the image if found, otherwise the regular icon is shown.

### DischargingMessageResourceKey ###
Type: **String**
Default value: **DischargingMessage**
Dependencies: **ManuallyCompartimentalize**

Translation resource key of the message to display to the driver if *ManuallyCompartimentalize* is set to *true*.

### DischargingMessageTimeoutInSeconds ###
Type: **Integer**
Default value: **5**
Dependencies: **DischargingConfirmMessageRequired**, **ManuallyCompartimentalize**

Time, in seconds, that the message telling the driver to go to dispatch, is displayed on screen, before it automatically disappears. This setting has no effect when *DischargingConfirmMessageRequired* is set to *false*.

### ManuallyCompartimentalize ###
Type: **Boolean**
Default value: ***true***

If set to *true*, the dispatcher will need to compartmentalise the work order outside of the kiosk. If set to *false*, the driver will compartmentalise the work order himself.

### ResolveEquipment ###
Type: **Boolean**
Default value: ***false***

Indication if the equipment resolver will be enabled before the compartmentaliser will do its job.

## DispenserFlow ##
### BadgeDispenserRequestTopic ###
Type: **String**
Default value: ***null***

The topic that is used to publish *DispenseBadgeRequestMsg*-messages to to request a badge from the dispenser.

### BadgeDispenserSubscriberTopic ###
Type: **String**
Default value: ***null***

The topic that is being subscribed to to listen for *BadgeDispensedMsg*- and *DispenseFailedMsg*-messages.

### CanAttachIdentificationToDriver ###
Type: **Boolean**
Default value: ***false***

Indication if the dispensed badge will be attached to the user.

### CanAttachIdentificationToWorkOrder ###
Type: **Boolean**
Default value: ***false***

Indication if the dispensed badge will be attached to the work order of the user.

### DispenserImage ###
Type: **String**
Default value: ***null***

Name of an image that is located in the *'Resources'*-folder. It shows the image if found, otherwise the regular icon is shown.

### DispenserTimeoutInSeconds ###
Type: **Nullable Integer**
Default value: ***null***

Represents a duration, in seconds, for the dispenser to respond to the *DispenseBadgeRequestMsg*-message. If set, an error will be shown to the user if the duration has passed without response from the dispenser. If not set, the flow waits until a response from the dispenser is returned.

## DriverIdentificationFlow ##
### AskDriverCompany ###
Type: **Boolean**
Default value: ***false***

Indication if the company for which the driver works will be requested.

### AverageDriverAge ###
Type: **Integer**
Default value: **30**

*Not used.*

### CanAttachIdentification ###
Type: **Boolean**
Default value: ***false***

Indication if the identification that was used to trigger the flow can be attached to the driver.

### CanCreateNewDriver ###
Type: **Boolean**
Default value: ***false***

Indication if a new driver can be created if the details entered are not known as a current driver.

### CanCreateWO ###
Type: **Boolean**
Default value: ***false***
Dependencies: **CanCreateNewDriver**

Indication if a work order can be created if a work order was not identified before. This setting is only considered when *CanCreateNewDriver* has a value of *true*.

### DriverAgeRange ###
Type: **Integer**
Default value: **100**
Dependencies: **MinDriverAge**

Used to initialise the keyboard for entering the driver's date of birth. The year-range for the entry of the date of birth will go from *(Current Year - MinDriverAge - DriverAgeRange)* up to *(Current Year - MinDriverAge)*, where both ends of the range are inclusive.

### MandatoryLicenses ###
Type: **List<String\>**
Default value: **Empty list**

List of active licenses that a person needs to have in order for the flow to continue. Multiple licenses can be specified and need to be separated by a pipe-character ('|').

Example:

* Setting: '`Driver's License|ADR`'
* Result: The person needs to have a valid driver and ADR license.

### MandatorySkills ###
Type: **Dictionary<String, Nullabele Integer>**
Default value: **Empty dictionary**

List of skills that a person needs to have in order for the flow to continue. Multiple skills can be specified and need to be separated by a pipe-character ('|'). Each skill can have a minimum skill level defined.

Example:

* Setting: '`Unloading|Loading:3`'
* Result: The person needs to have the skill '*Unloading*', and this can be of any level. The person also needs the skill '*Loading*', but needs to be at least at level 3 to be acceptable.

### MinDriverAge ###
Type: **Integer**
Default value: **18**

Used to initialise the keyboard for entering the driver's date of birth. The year-range for the entry of the date of birth will go from *(Current Year - MinDriverAge - DriverAgeRange)* up to *(Current Year - MinDriverAge)*, where both ends of the range are inclusive.

### ValidateMandatoryDriverLicenses ###
Type: **Boolean**
Default value: ***false***
Dependencies: **MandatoryLicenses**

Indication if the mandatory licenses defined by *MandatoryLicenses* need to be validated.

### ValidateMandatoryDriverSkillLevel ###
Type: **Boolean**
Default value: ***false***
Dependencies: **MandatorySkills**

Indication if the person's skills defined by *MandatorySkills* need to be validated for their existence and level.

## DriverInformationFlow ##
### MandatoryLicenses ###
Type: **List<String\>**
Default value: **Empty list**

List of active licenses that a person needs to have in order for the flow to continue. Multiple licenses can be specified and need to be separated by a pipe-character ('|').

Example:

* Setting: '`Driver's License|ADR`'
* Result: The person needs to have a valid driver and ADR license.

### MandatorySkills ###
Type: **Dictionary<String, Nullabele Integer>**
Default value: **Empty dictionary**

List of skills that a person needs to have in order for the flow to continue. Multiple skills can be specified and need to be separated by a pipe-character ('|'). Each skill can have a minimum skill level defined.

Example:

* Setting: '`Unloading|Loading:3`'
* Result: The person needs to have the skill '*Unloading*', and this can be of any level. The person also needs the skill '*Loading*', but needs to be at least at level 3 to be acceptable.

### ShowErrorWhenDeclined ###
Type: **Boolean**
Default value: ***false***

Indication if a message is displayed when the person details are found to be incorrect. When set to *true* and the person details are not accepted, a message informing the person to go to dispatch is displayed. When set to *false* and the person details are not accepted, the flow navigates back to the previous screen.

### ValidateMandatoryDriverLicenses ###
Type: **Boolean**
Default value: ***false***
Dependencies: **MandatoryLicenses**

Indication if the mandatory licenses defined by *MandatoryLicenses* need to be validated.

### ValidateMandatoryDriverSkillLevel ###
Type: **Boolean**
Default value: ***false***
Dependencies: **MandatorySkills**

Indication if the person's skills defined by *MandatorySkills* need to be validated for their existence and level.

## ExecutionRequirementsFlow ##
### AutoCompUnavailableCompartmentsWithAdrProduct ###
Type: **Boolean**
Default value: ***true***

Indication if compartmentalisation will be done by the driver or the dispatcher when the vehicle of the driver already contains ADR products. When set to *true*, the flow continues and the driver will perform the compartmentalisation. When set to *false*, a message informing the driver to go to dispatch is displayed and thus the dispatcher will perform the compartmentalisation.

## FirstWeighingFlow ##
### CheckPositioningWhenWeighing ###
Type: **Boolean**
Default value: ***false***

Indication if the position of the truck needs to be verified prior to weighing.

### ConnectedWeighingBridgeIdentifier ###
Type: **String**
Default value: ***null***

The name of the weighbridge where the kiosk is installed at.

### InitiateWeighingMsgPublishTopic ###
Type: **String**
Default value: ***null***

The topic that is being used to publish *InitiateWeighingMsg*-messages to, to initiate the weighing process.

### PositionResultMsgSubscriberTopic ###
Type: **String**
Default value: ***null***
Dependencies: **CheckPositioningWhenWeighing**

The topic that is being subscribed to, to listen for vehicle positioning results (*PositionResultMsg*). This is only used when *CheckPositioningWhenWeighing* is set to *true*.

### PositionStatusMsgPublishTopic ###
Type: **String**
Default value: ***null***
Dependencies: **CheckPositioningWhenWeighing**

The topic that is being used to publish *PositionStatusMsg*-messages to, to initiate the vehicle positioning check. This is only used when *CheckPositioningWhenWeighing* is set to *true*.

### WeighingResultMsgSubscriberTopic ###
Type: **String**
Default value: ***null***

The topic that is being subscribed to, to listen for weighing results (*WeighingResultMsg*).

### WeighingTimeoutInSeconds ###
Type: **Integer**
Default value: **30**

Timeout, in seconds, that the weighing process can last, before an error message is displayed on screen.

## LanguageSelectionFlow ##
### AutoAcceptWhenLanguageSelected ###
Type: **Boolean**
Default value: ***false***

Indication if the language selection needs to be confirmed. When set to *true*, the flow will continue at once when a language is selected by the user. When set to *false*, the user will need to confirm his/her language selection. This gives the opportunity to the user to change the language if the selected language was incorrect.

## MeansOfTransportFlow ##
### AdrCertificateValidityInYears ###
Type: **Integer**
Default value: **5**
Dependencies: **CanCreateMeansOfTransportAdrCertificate**

Used to initialise the keyboard for entering the adr certificate's end date. The year-range for the entry of the certificate's end date will go from *(Current Year)* up to *(Current Year + AdrCertificateValidityInYears)*, where both ends of the range are inclusive.

### CanCreateMeansOfTransport ###
Type: **Boolean**
Default value: ***false***

Indication if a means of transport, be it truck, trailer, etc., can be created when an unknown identifier is entered by the driver.

### CanCreateMeansOfTransportAdrCertificate ###
Type: **Boolean**
Default value: ***false***

Indication if an ADR-certificate can be created if no valid ADR-certificate was found for the means of transport part that is being identified. This setting is only used when an ADR-certificate is required by the product(s) that will be loaded.

### MeansOfTransportCombinationTypes ###
Type: **List<MeansOfTransportCombinationTypes\>**
Default value: ***Empty list***

List of means of transport combination types that will be available for selection by the driver. Multiple means of transport combination types can be specified and need to be separated by a pipe-character ('|').

Example:

* Setting: '`TractorTrailer|TractorContainer|TractorContainerFlex`'
* Result: The driver will be presented with the combinations types *Tractor/Trailer*, *Tractor/Container* and *Tractor/Container with flexibag*.

## MessageFlow ##
### ConfirmMessageRequired ###
Type: **Boolean**
Default value: ***false***

Indication if the message is displayed for a specified amount of time, or until the user confirms it.

### MessageImage ###
Type: **String**
Default value: ***null***

Name of an image that is located in the *'Resources'*-folder. It shows the image if found, otherwise the regular icon is shown.

### MessageResourceKey ###
Type: **String**
Default value: ***null***

Resource name of the message to display to the user.

### MessageTimeoutInSeconds ###
Type: **Integer**
Default value: **10**
Dependencies: **ConfirmMessageRequired**

Time, in seconds, that the message is displayed on screen, before the flow continues. This is only used when *ConfirmMessageRequired* is set to *false*.

## MoveWoToLocationFlow ##
### MoveWoToLocation_LocationName ###
Type: **String**
Default value: ***null***

Name of the location to move the work order to.

### MoveWoToLocation_PublishTopic ###
Type: **String**
Default value: ***null***

The topic that is being used to publish *WorkOrderLocationChangeRequestMsg*-messages to, to initiate the work order location change.

## OrderIdentificationFlow ##
### CheckSoeHasDestination ###
Type: **Boolean**
Default value: ***true***

### CheckTimeSlot ###
Type: **Boolean**
Default value: ***true***

Indication if the timeslot of a sales order entry is validated when it is selected for execution by the driver.

### CheckValidityPeriod ###
Type: **Boolean**
Default value: ***true***

Indication if the validity period of a sales order entry is validated when it is selected for execution by the driver; if any was specified.

### TimeSlotBeforeDeviationInMinutes ###
Type: **Integer**
Default value: **0**
Dependencies: **CheckTimeSlot**

Time, in minutes, that the driver can start to prepare a date-dependent sales order entry before the start of its timeslot.

Example:

* Sales order entry: Date-dependant and timeslot is set from 1/1/2018 11:30 until 1/1/2018 13:30.
* Setting: '`30`'
* Result: The driver can select the sales order entry for execution on 1/1/2018 from 11:00. If the driver selects the sales order entry before this time, an error will be shown.

### TimeSlotEndDeviationInMinutes ###
Type: **Integer**
Default value: **0**
Dependencies: **CheckTimeSlot**

Time, in minutes, that the driver can start to prepare a date-dependent sales order entry after the end of its timeslot.

Example:

* Sales order entry: Date-dependant and timeslot is set from 1/1/2018 11:30 until 1/1/2018 13:30.
* Setting: '`30`'
* Result: The driver can select the sales order entry for execution on 1/1/2018 until 14:00. If the driver selects the sales order entry after this time, an error will be shown.

## PincodeIdentificationFlow ##
### PincodeIdentification_CanCheckForVisitor ###
Type: **Boolean**
Default value: ***false***

Indication if validation occurs to verify that the pincode belongs to a person, what type of person and that it is an active identification of that person and that the person isn't locked or disabled.

### PincodeIdentification_UseRoutingParams ###
Type: **Boolean**
Default value: ***false***
Dependencies: **PincodeIdentification_CanCheckForVisitor**

Indication if routing parameters wil be used to divert from the regular flow. This is only used when *PincodeIdentification_CanCheckForVisitor* is set to *true*.

## PrintDocumentFlow ##
### PrintDocument_NumberOfCopies ###
Type: **Integer**
Default value: **1**

Number of times the document will be printed.

### PrintDocument_OverrideLanguageIs2Code ###
Type: **String**
Default value: ***null***

The language used to print documents. If not specified, a document is printed using the driver's language. When that is not specified, the active culture of the kiosk is used.

### PrintDocument_PrinterName ###
Type: **String**
Default value: ***null***

The name of the printer to print documents.

### PrintDocument_PublishTopic ###
Type: **String**
Default value: ***null***
Dependencies: ****

The topic that is used to publish *DocumentPrintRequestMsg*-messages to, to request a document to be printed.

### PrintDocument_ReportName ###
Type: **String**
Default value: ***null***

The name of the document to print.

### PrintDocument_SubscribeTopic ###
Type: **String**
Default value: ***null***
Dependencies: ****

The topic that is being subscribed to, to listen for *DocumentPrintedMsg*-messages that indicate that a document has been printed.

### PrintDocument_TimeoutInSeconds ###
Type: **Integer**
Default value: **10**

Timeout, in seconds, to wait for the print response after the print request is sent.

## ProjectFlow ##
*No configurable settings.*

## ReleaseNextWorkOrderEntryFlow ##
*No configurable settings.*

## ResetFlow ##
*No configurable settings.*

## ResetWorkOrderFlow ##
### RemoveDriver ###
Type: **Boolean**
Default value: ***false***

Indication if the driver needs to be removed from the work order.

### RemoveMOT ###
Type: **Boolean**
Default value: ***false***

Indication if the means of transport needs to be removed from the work order.

## SafetyRequirementsFlow ##
### SafetyHazardsIcons ###
Type: **List<String\>**
Default value: **Empty list**

List of hazard icon names of the ISO-7010 standard as specified on [Wikipedia](https://commons.wikimedia.org/wiki/Category:ISO_7010_safety_signs_(vector_drawings)). Multiple icon names can be specified and need to be separated by a pipe-character ('|'). The flow shows the icons, 8 at a time.

### SafetyRequirementsIcons ###
Type: **List<String\>**
Default value: **Empty list**

List of safety requirement icon names of the ISO-7010 standard as specified on [Wikipedia](https://commons.wikimedia.org/wiki/Category:ISO_7010_safety_signs_(vector_drawings)). Multiple icon names can be specified and need to be separated by a pipe-character ('|'). The flow shows the icons, 8 at a time.

### SafetyRestrictionsIcons ###
Type: **List<String\>**
Default value: **Empty list**

List of safety restriction icon names of the ISO-7010 standard as specified on [Wikipedia](https://commons.wikimedia.org/wiki/Category:ISO_7010_safety_signs_(vector_drawings)). Multiple icon names can be specified and need to be separated by a pipe-character ('|'). The flow shows the icons, 8 at a time.

## ShowImagesFlow ##
### ImageNames ###
Type: **List<String\>**
Default value: **Empty list**

List of image names that are located in the *'Resources/SafetyImages'*-folder. Multiple images can be specified and need to be separated by a pipe-character ('|'). The flow shows the images, 8 at a time.

## SolutionProposalFlow ##
### CanAddAndResolveTransfers ###
Type: **Boolean**
Default value: ***false***

Indication if transfers will be created and added to the generated compartment order entries.

### CanReplaceDestinationEquipment ###
Type: **Boolean**
Default value: ***false***
Dependencies: **ResolveEquipment**

Indication if the equipment resolver can replace the destination equipment(s) of a work order entry. This setting has no effect when *ResolveEquipment* is set to *false*.

### CanReplaceSourceEquipment ###
Type: **Boolean**
Default value: ***false***
Dependencies: **ResolveEquipment**

Indication if the equipment resolver can replace the source equipment(s) of a work order entry. This setting has no effect when *ResolveEquipment* is set to *false*.

### MaxNumberOfProposals ###
Type: **Integer**
Default value: **10**

Maximum number of solutions proposals for the compartmentaliser to return. The number of solution proposals can be less than *MaxNumberOfProposals*.

### ResolveEquipment ###
Type: **Boolean**
Default value: ***false***

Indication if the equipment resolver will be enabled before the compartmentaliser will do its job.

## TransitCountriesFlow ##
### DefaultSelectedTransportationType ###
Type: **List<TransportationTypes\>**
Default value: ***Empty list***

List of transportation types that will be selected by default. Multiple transportation types can be specified and need to be separated by a pipe-character ('|').

Example:

* Setting: '`Truck|Naval`'
* Result: When the complete list of transportation types is displayed, the transportation types *Truck* and *Naval* will be pre-selected.

## VirtualReaderFlow ##
### VirtualReader_PublishTopic ###
Type: **String**
Default value: ***null***

The topic that is used to publish *VirtualTerminalReadEventMsg*-messages to, to imitate a read from a physical access control device.

### VirtualReader_SubscriberTopic ###
Type: **String**
Default value: ***null***

The topic that is being subscribed to, to listen for *BadgeRejectedMsg*- and *BadgeRegisteredMsg*-messages that indicate unsuccessful and successful reads from a physical access control device respectively.

### VirtualReader_TimeOut ###
Type: **Integer**
Default value: **30**

Timeout, in seconds, to wait for the response of a virtual badge read.

## VisitorIdentificationFlow ##
### CanUseVehicle ###
Type: **Boolean**
Default value: **false**

Indication if the flow will request to identify the user's vehicle.

### IdentificationCodeKeyboard ###
Type: **String**
Default value: **null**

Configuration of the keyboard to be used for entering the identification code.

### MaxLengthIdentificationCode ###
Type: **Integer**
Default value: **16**

Indicates the maximum number of characters a user can enter as a identification code.

## WeighingFlow ##
### CanAutoInvalidateFirstWeighing ###
Type: **Boolean**
Default value: ***true***

Indication if the first weighing can be re-done. For this, none of the work order entries can have a compartment order entry.

### CheckPositioningWhenWeighing ###
Type: **Boolean**
Default value: ***false***

Indication if the position of the truck needs to be verified prior to weighing.

### CheckTransferStates ###
Type: **Boolean**
Default value: ***false***

Indication if a verification needs to be done to check for transfers that are in progress.

### ConfirmWeighingTimoutInSeconds ###
Type: **Integer**
Default value: ***null***

Time, in seconds, in which the weighing result needs to be confirmed before the flow continues automatically. When not set, the user will need to confirm the weighing result. This gives the opportunity to the user to redo the weighing.

### ConnectedWeighingBridgeIdentifier ###
Type: **String**
Default value: ***null***

The name of the weighbridge where the kiosk is installed at.

### initiateWeighingMsgPublishTopic ###
Type: **String**
Default value: ***null***

The topic that is being used to publish *InitiateWeighingMsg*-messages to, to initiate the weighing process.

### PerformPostLoadingCheck ###
Type: **Boolean**
Default value: ***false***

Indication if a number of checks will take place, such as for under- or overfill, maximum allowed gross weight, etc.

### PositionResultMsgSubscriberTopic ###
Type: **String**
Default value: ***null***
Dependencies: **CheckPositioningWhenWeighing**

The topic that is being subscribed to, to listen for vehicle positioning results (*PositionResultMsg*). This is only used when *CheckPositioningWhenWeighing* is set to *true*.

### PositionStatusMsgPublishTopic ###
Type: **String**
Default value: ***null***
Dependencies: **CheckPositioningWhenWeighing**

The topic that is being used to publish *PositionStatusMsg*-messages to, to initiate the vehicle positioning check. This is only used when *CheckPositioningWhenWeighing* is set to *true*.

### WeighingRegisteredMsgPublishTopic ###
Type: **String**
Default value: ***null***

The topic that is being used to publish *WeighingRegisteredMsg*-messages to, to indicate that a weighing has happened to any interested parties.

### WeighingResultMsgSubscriberTopic ###
Type: **String**
Default value: ***null***

The topic that is being subscribed to, to listen for weighing results (*WeighingResultMsg*).

### weighingTimeoutInSeconds ###
Type: **Integer**
Default value: **30**

Timeout, in seconds, that the weighing process can last, before an error message is displayed on screen.