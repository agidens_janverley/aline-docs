[Home](../Home)

# Portal

## Translation 

As of Epia version 5, translations for the portal can be retrieved using the new `MixedResourceService`; which can handle translations from both *.resources*-files and the database. Configuration changes that are needed:

- References to Epia-dll's in Egemin.Epia.Server.exe.config need to be updated from version 4.0.0.0 to 5.0.0.0.
- Update the service type for interface `Egemin.Epia.Foundation.Globalization.Interfaces.IResourceService` in Egemin.Epia.Server.Default.WcfServiceDescription from `Egemin.Epia.Components.Globalization.FileBasedResourceService` to `Egemin.Epia.Components.Globalization.MixedResourceService`
- Update the component configuration for the resource service in Egemin.Epia.Server.exe.config from

```
<componentinstance uid="1" blueprinttype="Egemin.Epia.Components.Globalization.FileBasedResourceService, Egemin.Epia.Components" sequencenumber="0">
    <parameters>
        <parameter name="ConfigurationDirectory" value="Data\Resources\"/>
        <parameter name="ResourcesExtension" value=".resources"/>
        <parameter name="DefaultLanguage" value="en"/>
    </parameters>
</componentinstance>
```

to
```
<componentinstance uid="1" blueprinttype="Egemin.Epia.Components.Globalization.MixedResourceService, Egemin.Epia.Components" sequencenumber="0">
    <parameters>
        <parameter name="ConfigurationDirectory" value="Data\Resources\"/>
        <parameter name="ResourcesExtension" value=".resources"/>
        <parameter name="DefaultLanguage" value="en"/>
        <parameter name="ConnectionString" value="Data Source=.;Initial Catalog=MegaVitaDev;Integrated Security=True;MultipleActiveResultSets=True"/>
        <parameter name="EnableFileBasedResources" value="false"/>
        <parameter name="EnableDbBasedResources" value="true"/>
    </parameters>
</componentinstance>
```

**Note**
> Make sure that the server on which Epia Server is installed has access to the Aline database.

## Terminal Overview
The terminal overview presents work orders based on their current location. Each location is represented by its own control, and keeps track of all work orders for that location. It is possible to move a work order to another location by using the context menu and selecting a new location. Selecting a new location for a work order will send a location change request message (`WorkOrderLocationChangeRequestMsg`) to the [Access Control service](../services/AC) which in turn will move the work order and send a location changed message (`WorkOrderLocationChangeRegisteredMsg`) to which the terminal overview screen is listening. If such a message is received from the access control service, the location-controls that are involved in the location change (2 at most), will be updated.

## [Settings](settings)

## Disable License Check

Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Egemin\EPIA]
"SuppressExitDueToLicense"=dword:00000001