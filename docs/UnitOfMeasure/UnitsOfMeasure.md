## Units Of Measure

### Why units of measure

Units of measure are put in place to strongly type values that represent different types of measure.
This avoids using wrong values in calculations.
To have the mass air for example, it forces you to use the density air. If you use density vac by mistake, the compiler will refuse to build.

### The Quantity container

Each quantity has its own struct that inherits the IQuantity interface.
A quantity struct is decorated with some attributes, mostly used to load htem by reflection.

The attribute **QuantityDescription** Sets the name of the quantity and a brief description.
The **QuantityType** attribute sets a **QuantityType** property.

The QuantityType is a fixed enumeration that categorizes the quantities.
Available values are:
- Volume
- Temperature
- Mass
- Length
- Density

Both Attributes are mandatory

#### Units of a Quantity

A quantity must have at least one and can have multiple Units.
A unit is decorated with the symbol and the UnitOfMeasureType, valid for this unit. One of the units should be set as the default unit for this quantity.
```
[Unit("m^3", UnitOfMeasureTypes.m3GOV, true)]
public static VolumeGOV CubicMetre { get; } = new VolumeGOV(1);
```
Each static unit returns a new instance of the quantity initialized with the factor of the **base value**.

The base value is the actual value that is stored (the value property).
In case of VolumeGOV we can see that the quantity's base value is m^3^.

For example, 1 cm^3^ = 0.000001 m^3^:
```
[Unit("cm^3", UnitOfMeasureTypes.cm3GOV)]
public static VolumeGOV CubicCentimetre { get; } = new VolumeGOV(0.000001m);
```

You can use the units to create quantity instance from values very quick. 

If we have 354 of m^3^ and want to store it in a quuntity object:
```
var qtyObject = 354 * VolumeGOV.CubicMetre;
```
The same is true when it were cubic centimetres
```
var qtyObject = 354 * VolumeGOV.CubicCentimetre;
```

Each quantity type can have unlimited operators. For example, a volume can be multiplied by a density and returns a mass.
For these possiblities, please consult the documentation of each quantity.

### The IQuantity Interface

Each unit of measure inherits the IQuantity interface. This forces the unit of measure to implement the general methods for a quantity.

#### Methods of an IQuantity object

##### ToString()

The ToString method creates a string of the value for the default unit of measure for this quantity. The symbol is attached at the end of the string.
For example, the tostring of a quantity of 354 m^3^ of volumeGOV will give 
```
354 m^3
```

You can pass a UnitOfMeasureType as a parameter to force output as an other UnitOfMeasure.
```
qtyObject.ToString(UnitOfMeasureTypes.LGOV);
Output> 354000 L
```

There is also the add a parameter with a format string (default format string format), add a different format provider or even another unitprovider.


##### ConvertTo()

Beside outputting the value as a string, we can also get the value at a specified unit as decimal.
By calling the **ConvertTo()** method you get a decimal of the request unit that has been set as parameter.
```
var qtyObject = 345 * VolumeGOV.CubicMetre;
var litre = qtyObject.ConvertTo(VolumeGOV.Litre);
Console.WriteLine(litre);
output> 345000
```

##### MultiplyBy

The quantity interface has also a Multiply method. It returns a new quantity object with the current value multiplied with the parameter's value.

##### Add

A method to add a quantity object (of the same type) the result is a new quantity object with the sum of the two values.

#### addition interfaces

The IQuantity interface also inherits from the following interfaces, *IComparable, IFormattable, IXmlSerializable*
These interfaces are implemented so Equals and Compare things work.

#### Standard operators

Each unit has default operators implemented. 

##### Operators with left and right parameters of the units' type

+, -, /, ==, !=, >, <, >=, <=

##### Operators with one parameter of the units' type and one of the decimal type

*, /



