## TOV to GOV conversion

This convertor converts a Volume of type TOV to a Volume of type GOV.

### Init

To load the convertor, create a new instance of the TOVtoGOV class. Namespace: *Agidens.Terminal.Suite.Shared.UnitOfMeasure.Conversions*

```
var convertor = new TOVtoGOV();
```

#### SourceType

The source type of the convertor is **VolumeTOV**

#### DestinationType

The destination type is **VolumeGOV**

### Conversion parameters

This convertor has 8 parameters

- The observed *Temperature* of the product
- The ambient *Temperature*. Only in case of insulated tanks. If not, temperature can be Temperature.Default
- the Shell reference *Temperature*. The reference temperature of the VCF or LTEC of the tank material.
- A *bool* indicating if the tank is insulated.
- A nullable *decimal* of the LTEC of the tank material. null if the VCF is set.
- A nullable *decimal* with the VCF of the tank. null if the LTEC is used.
- The *VolumeTOV* of free water and sediment.
- The *VolumeTOV* of floating roof adjustment


Either the VCF or the LTEC should be given. If both are null, no tank volume correction is done!

