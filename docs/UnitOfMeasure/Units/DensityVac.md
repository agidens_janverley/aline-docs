### DensityVac

This unit represents the density of a substance in Vacu�m.

#### Base unit

The internal value of DensityVac is Kg/m^3

#### Units

- Kg/m^3 *(default)* - 1 * basevalue
```
DensityVac.KilogramPerCubicMetre
UnitOfMeasureTypes.DensityKgm3Vac
```
- t/m^3 - 1000 * basevalue
```
DensityVac.TonnePerCubicMetre
UnitOfMeasureTypes.Densitytm3Vac
```
- Kg/L - 1000 * basevalue
```
DensityVac.KilogramPerLiter
UnitOfMeasureTypes.DensityKgLVac
```

#### Additional methods

###### Transform

Transform DensityAir to DensityVac
```
DensityVac air = DensityVac.Transform(air);
```

#### Additional operators

###### DensityVac * VolumeGOV

Multiplying DensityVac with a unit VolumeGOV results in a MassVac

```
MassVac mass = density * volume
```
