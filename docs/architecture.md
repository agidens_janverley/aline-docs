[Home](Home)
# Service Layer

## `ServiceResult<T>`

A ServiceResult<T\> can be returned from a service-call. This will contain the DTO of the entity for which the service-call was made (T), together with any validation errors that occurred during the call. The list of validation errors is comprised of all validation errors that occurred for all domain-calls that return an `IResult`.

# Domain Layer

## `IResult`

Concrete implementations: `Result` and `CompositeResult`.

Gives an indication which validation steps failed. All possible validations can be consulted [here](iresult_validations).

# Hooks

There is a need for external parties to be notified when actions occur in the Aline core in order to respond to them.

The current implementation resembles *Version 1* of the illustration below where only the service layer has the ability to notify of actions. In the future, this should be extended to *Version 2* of the illustration below where the domain queues action notifications and the service layer will publish those notifications when the transaction to the storage layer has completed successfully. This allows for more fine-grained notifications.

![Project hooks](./Resources/Architecture/ProjectHooks.png)

The supported actions include:

- Created
- Updated
- Deleted
- CreatedOnImport
- UpdatedOnImport
- DeletedOnImport

At the moment, notifications are only triggered in the following scenarios:

- Creation of a sales order
- Removal of a sales order
- Creation of a sales order entry
- Removal of a sales order entry
- Creation of a sales order during ERP-import
- Change of a sales order during ERP-import
- Removal of a sales order during ERP-import

***Note:***

> When removing a sales order, the sales order entries are removed using cascading which means that in the *Version 1*-implementation there will only be 1 notification sent out for the removal of a sales order and none for the possible sales order entries that were linked to it.

The notifications are sent using RabbitMQ-messages of type *ActionResultMsg* with the following details:
- Type of action
- Type of the entity on which the action occurred
- Unique identifier of the entity on which the action occurred

These RabbitMQ-messages will be published to a topic representing a functional block as described below.

A topic is constructed in the following manner:
'Aline.ActionResult.`{SubLevel}`.`{Entity type}`.`{Action}`.`{Unique identifier}`'.

The sub-level is defined by the type of entity:

| Sub-level        | Entity Type               |
| ---------------- | ------------------------- |
| Orders.Planning  | SalesOrder                |
|                  | SalesOrderEntry           |
| Orders.Execution | WorkOrder                 |
|                  | WorkOrderEntry            |
|                  | CompartmentOrderEntry     |
|                  | Transfer                  |
|                  | WorkOrderMeansOfTransport |
| Administration   | Equipment                 |
|                  | MeansOfTransport          |

**Example:**

Topic for a work order entry with id 345 which is updated will be *Aline.ActionResult.Orders.Execution.WorkOrderEntry.Updated.345*.