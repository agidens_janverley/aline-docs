### Temperature

This unit represents a temperature.

#### Base unit

The internal value of Temeprature is Kelvin

#### Units

- �C *(default)* - basevalue - Absolute Zero (273.15)
```
Temperature.DegreeCelsius
UnitOfMeasureTypes.DegreesCelsius
```
- �F - ((basevalue - Absolute Zero (273.15)) * 9 / 5) + 32
```
Temperature.DegreeFahrenheit
UnitOfMeasureTypes.DegreesFahrenheit
```
- K - 1 * basevalue
```
Temperature.Kelvin
UnitOfMeasureTypes.DegreesKelvin
```

#### Additional methods

none

#### Additional operators

none
