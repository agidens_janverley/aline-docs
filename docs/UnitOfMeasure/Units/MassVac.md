### MassVac

This unit represents a Mass of a certain amount of substance in vacu�m.

#### Base unit

The internal value of MassVac is Kg.

#### Units

- Kg *(default)* - 1 * basevalue
```
MassVac.Kilogram
UnitOfMeasureTypes.KgVac
```
- g - 0.001 * basevalue
```
MassVac.Gram
UnitOfMeasureTypes.gramVac
```
- t - 1000 * basevalue
```
MassVac.Tonne
UnitOfMeasureTypes.tVac
```
- lt - 984.206 * basevalue
```
MassVac.LongTon
UnitOfMeasureTypes.ltVac
```

#### Additional methods

###### ConvertTo

Transform MassAir to MassVac
```
MassVac vac = MassVac.Transform(air);
```

#### Additional operators

###### MassVac / VolumeGOV

Dividing MassVac with a unit VolumeGOV results in a DensityVac

```
DensityVac dens = mass / volume
```

###### MassVac / VolumeGSV

Dividing MassVac with a unit VolumeGSV results in a DensityVacAtRef

```
DensityVacAtRef dens = mass / volume
```

###### MassVac / DensityVacAtRef

Dividing MassVac with a unit DensityVacAtRef results in a VolumeGSV

```
VolumeGSV volume = mass / dens
```
