[Home](home)

- [AL-967](http://10.10.14.20:8081/issue/AL-967) - Inventory Tank  detail - density not show but is present
- [AL-1109](http://10.10.14.20:8081/issue/AL-1109) - Operations Instructions - Show all instructions
- [AL-1110](http://10.10.14.20:8081/issue/AL-1110) - Bill of lading - review project specific values
- [AL-1123](http://10.10.14.20:8081/issue/AL-1123) - WCF call wrapper timeout set too high  
        WCF calls now timeout after 20 seconds.
- [AL-1173](http://10.10.14.20:8081/issue/AL-1173) - Operation instructions - general remarks
- [AL-1175](http://10.10.14.20:8081/issue/AL-1175) - CR - Project Handles
        New setting 'MessageBrokerHost' defined in the app.config of the Aline backend services.
- [AL-1176](http://10.10.14.20:8081/issue/AL-1176) - Migrate Translation engine
        Translations are stored in the database.
- [AL-1177](http://10.10.14.20:8081/issue/AL-1177) - User Manual pilot
        To be filled in when `To Verify`