### VolumeGSV

This unit represents Gross Standard Volume.

#### Base unit

The internal value of VolumeGSV is Kg/m^3

#### Units

- m^3 *(default)* - 1 * basevalue
```
VolumeGSV.CubicMetre
UnitOfMeasureTypes.m3GSV
```
- dm^3 - 0.001 * basevalue
```
VolumeGSV.CubicDecimetre
UnitOfMeasureTypes.dm3GSV
```
- cm^3 - 0.000001 * basevalue
```
VolumeGSV.CubicCentimetre
UnitOfMeasureTypes.cm3GSV
```
- mm^3 - 0.000000001 * basevalue
```
VolumeGSV.CubicMillimetre
UnitOfMeasureTypes.mm3GSV
```
- L - 0.001 * basevalue
```
VolumeGSV.Litre
UnitOfMeasureTypes.LGSV
```
- gal (US) - 0.003785411784 * basevalue
```
VolumeGSV.GallonUS
UnitOfMeasureTypes.GalUSGSV
```
- gal (imp) - 0.00454609 * basevalue
```
VolumeGSV.GallonImp
UnitOfMeasureTypes.galimpGSV
```
- Barrel - 0.158987294928 * basevalue
```
VolumeGSV.BarrelBbl
UnitOfMeasureTypes.bblGSV
```

#### Additional methods

none

#### Additional operators

###### VolumeGSV * DensityAirAtRef

Multiplying VolumeGSV with a unit DensityAirAtRef results in a MassAir

```
MassAir mass = volume * density
```

###### VolumeGSV * DensityVacAtRef

Multiplying VolumeGSV with a unit DensityVacAtRef results in a MassVac

```
MassVac mass = volume * density
```
