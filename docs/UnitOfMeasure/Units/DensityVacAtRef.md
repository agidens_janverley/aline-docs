### DensityVacAtRef

This unit represents the density of a substance in vacu�m at the reference temperature.

#### Base unit

The internal value of DensityAirAtRef is Kg/m^3

#### Units

- Kg/m^3 *(default)* - 1 * basevalue
```
DensityVacAtRef.KilogramPerCubicMetre
UnitOfMeasureTypes.DensityKgm3VacAtRef
```
- t/m^3 - 1000 * basevalue
```
DensityVacAtRef.TonnePerCubicMetre
UnitOfMeasureTypes.Densitytm3VacAtRef
```
- Kg/L - 1000 * basevalue
```
DensityVacAtRef.KilogramPerLiter
UnitOfMeasureTypes.DensityKgLVacAtRef
```

#### Additional methods

###### ConvertTo Transform

Transform DensityAirAtRef to DensityVacAtRef
```
DensityVacAtRef vac = DensityVacAtRef.ConvertTo(air);
DensityVacAtRef vac2 = DensityVacAtRef.Transform(air);
```

#### Additional operators

###### DensityVacAtRef * VolumeGSV

Multiplying DensityVacAtRef with a unit VolumeGSV results in a MassVac

```
MassVac mass = density * volume
```
