![Agidens Logo](./Resources/Aline.png)

# Aline

[Architecture](architecture)

- [System Configuration](SystemConfiguration)
- [Database migrations](DatabaseMigration)
- [Unit of Measure](UnitOfMeasure/home)

[Reporting](reporting/home)

[Portal](portal/home)

[Kiosk](kiosk/home)

[Services](services/home)

[Release Notes](Aline/ReleaseNotes/home)

[Unresolved Bugs](Aline/Unresolved_Bugs)

## Wiki features

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax.

## Syntax highlighting

You can also highlight snippets of text (we use the excellent [Pygments][] library).

[Pygments]: http://pygments.org/


Here's an example:

```
#!csharp

internal class Test {

}
```