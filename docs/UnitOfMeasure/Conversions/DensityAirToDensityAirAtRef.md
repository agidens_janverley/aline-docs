## DensityAir to DensityAirAtRef conversion

This convertor converts a Density of type Air to a Density of type AirAtRef.

### Init

To load the convertor, create a new instance of the DensityAirToDensityAirAtRef class. Namespace: *Agidens.Terminal.Suite.Shared.UnitOfMeasure.Conversions*

```
var convertor = new DensityAirToDensityAirAtRef();
```

#### SourceType

The source type of the convertor is **DensityAir**

#### DestinationType

The destination type is **DensityAirAtRef**

### Conversion parameters

This convertor has 7 parameters

- The observed *Temperature* of the product
- The *AstmTable* conversion table to use.
- The Volume Correction Factor *decimal* of the product. (only for linear VC) Should be null when not applicable.
- The reference *Temperature* of the product. (only for linear VC) Should be null when not applicable.
- The K0 factor of the product. Can be null when conversion table != ASTM54B
- The K1 factor of the product. Can be null when conversion table != ASTM54B
- The number of decimals to be used for the VCF as a *int* when null, defaults to 5


Using the default number of decimals should be avoided. Give the system wide parameter.
Nullable parameters should be set as null. The list of parameters should contain 8 parameters.

