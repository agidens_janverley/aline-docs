[Home](home)

# Loading service

This service communicates with the level 2 PLC to provide the plc with the order data.
lvl2 is the initator and we respond with the data.

The communication flow is described in the following documents:

[Communication Diagram](https://agidens.sharepoint.com/sites/collaboration/ALINE-AXCEL/Aline%20Development/lvl2%20OrderBase%20Loading/Flowchart%20Order%20Based%20Loading.vsdx?d=w246e8767753840898c9f3502ec5e5a13)

[Message details](https://agidens.sharepoint.com/sites/collaboration/ALINE-AXCEL/Aline%20Development/lvl2%20OrderBase%20Loading/Communicatiestructuur%20Aline%20Rockwell.xlsx?d=w58c905d245544130b18ad485580ea56e)

## configuration of the service

The loading service has several configuration parameters

### InDemoMode

Enable demo data. When this option is true, the service runs stand alone and responds to request with dummy data.

### LoadingInfo_Setup_MaxSupportedCompartments

Sets the maximum number of supported compartments by the PLC

### LoadingInfo_Setup_MaxSupportedBlending

Sets the maximum amount of belding (simultaneous transfers to the same compartment) supported by the PLC. Set to 1 if blending is not supported.

### Messaging_PublishTopic_WorkOrderLocationRequest

Messagebus topic to send location change requests to

### Func_Enabled_Level2RequestsTokenInfo

Allow the PLC to request token info.

### Func_Enabled_Level2RequestsWoeInfo

Allow the PLC to request workorderentry info

### Func_Enabled_Level2RequestsCoeInfo

Allow the PLC to request Compartmentorderentry info

### Func_Enabled_Level2RequestsCoeStart

Allow the PLC to start a CompartmentOrderEntry

### Func_Enabled_Level2RequestsCoeUpdate

Allow the PLC to update the COE info

### Func_Enabled_Level2RequestsWoeUpdateSealNumbers

Allow the PLC to update seal numbers

### Func_Enabled_Level2RequestQualityUpdate

Allow the PLc to update Quality parameters of the compartment

### Qualitytypes

We can defined x quality paramters to be set by the PLC
We define types by adding a settings key "Mapping_QualityType_X" where x is the number of the type. The number starts by 1 for the first type and is increased by 1 for every next parametertype.
The value of the settings is the name of the qualityparameter as how it is defined in Aline (eg. TEMPERATURE or DENSITY...)

## OPC Tags

The following settings all have a OPC node id as value. The setting key defines the purpose of the OPC tag

*Token info request*

### LoadingInfo_TokenInfo_Level2To3

Sync status PLC -> ALINE

### LoadingInfo_TokenInfo_Level3To2

Sync status ALINE --> PLC

### LoadingInfo_TokenInfo_Request_Token

Tag that contains the token number the PLC needs info for.

*Token info response*

### LoadingInfo_TokenInfo_Response_Name

Name of the person attached to the token

### LoadingInfo_TokenInfo_Response_Language

Preferred language of the driver

### LoadingInfo_TokenInfo_Response_TrainingLevelOk

Flag is true when the driver has the training skill

### LoadingInfo_TokenInfo_Response_Type

The type of staff member:  1: Truckdriver 2: operator 4: Trainer (bit-logic)

### LoadingInfo_TokenInfo_Response_ErrorCode

In case of error, the error code, -1 if no error

### LoadingInfo_TokenInfo_Response_ErrorMessage

Human readable representation of the error

*Workorder Entry info request*

### LoadingInfo_Woe_Info_Level2To3

Sync tag PLC --> ALINE

### LoadingInfo_Woe_Info_Level3To2

Sync tag ALINE --> PLC

### LoadingInfo_Woe_Info_Request_Token

Token number of the driver

### LoadingInfo_Woe_Info_Request_ExecutorToken

Token number of the staff member executing, giving training. -1 if none

### LoadingInfo_Woe_Info_Request_LoadingSpot

Name of the loading spot the loading will take place. (equipment name)

### LoadingInfo_Woe_Info_Request_OrderNr

Ordernumber of the order to execute

*WorkOrderEntry info response *

### LoadingInfo_Woe_Info_Response_WoeId

The Aline internal Id of the workorderEntry

### LoadingInfo_Woe_Info_Response_Product

The name of the product to be loaded/unloaded

### LoadingInfo_Woe_Info_Response_State

The state of the workorderentry: Open = 1, ReadyForTransfer = 2, InProgress = 3, Completed = 4, Validated = 5,  Closed = 6, Aborted = 7, Deleted = 8

### LoadingInfo_Woe_Info_Response_HandlingType

Handling type of the workorderentry :  Undefined=0, TankToTruck=1, TankToShip=2, TankToBarge=3, TankToRTC=4,  TruckToTank=5, ShipToTank=6, BargeToTank=7, RTCToTank=8, TruckToTruck=9, ShipToShip=10, BargeToBarge=11, ShipToBarge=12, BargeToShip=13, TruckToRTC=14, RTCToTruck=15, RTCToRTC=16, TankToTank=17, Circulation=18, Tracing=19, PreBlending=20

### LoadingInfo_Woe_Info_Response_WoeMassKg

The planned mass in KgAir

### LoadingInfo_Woe_Info_Response_WoeVolumeL

The planned volume in L GSV

### LoadingInfo_Woe_Info_Response_MaxCompartments

The number of compartments used

### LoadingInfo_Woe_Info_Response_TransportationIds

Semi-colon seperated list of identifications of the means of transports

### LoadingInfo_Woe_Info_Response_ErrorCode

In case of error, the error code. -1 is no error

### LoadingInfo_Woe_Info_Response_ErrorMessage

A human readable representation of the error occured (if any)

### LoadingInfo_Woe_Info_Response_CoeInfo_CoeId_XX

The compartmentorderentry id (internal aline id) for the XX compartment of the workorder. XX is the number of the compartment. starts at 01 to *LoadingInfo_Setup_MaxSupportedCompartments* , represented by **two** digits.

### LoadingInfo_Woe_Info_Response_CoeInfo_MassKg_XX

The planned mass to be transferred to the compartment in KgAir. 
XX is the number of the compartment. starts at 01 to *LoadingInfo_Setup_MaxSupportedCompartments* , represented by **two** digits.

### LoadingInfo_Woe_Info_Response_CoeInfo_VolumeL_XX

The planned volume to be transferred to the compartment in L GSV. 
XX is the number of the compartment. starts at 01 to *LoadingInfo_Setup_MaxSupportedCompartments* , represented by **two** digits.

### LoadingInfo_Woe_Info_Response_CoeInfo_State_XX

The state of the compartmentorderentry. XX is the number of the compartment. starts at 01 to *LoadingInfo_Setup_MaxSupportedCompartments* , represented by **two** digits.
Open = 1, InProgress = 2, Completed = 3, Aborted = 4

*WorkOrderEntry update seal number request*

### LoadingInfo_Woe_UpdateSealNumbers_Level2To3

Sync tag PLC --> ALINE

### LoadingInfo_Woe_UpdateSealNumbers_Level3To2

Sync tag ALINE --> PLC

### LoadingInfo_Woe_UpdateSealNumbers_Request_WoeId

Workorder entry of which the seal numbers should be updated

### LoadingInfo_Woe_UpdateSealNumbers_Request_SealNumbersComp_XX

The actual seal number to be set. XX is the number of the compartment. starts at 01 to *LoadingInfo_Setup_MaxSupportedCompartments* , represented by **two** digits.

*WorkorderEntry update seal numbers response*

### LoadingInfo_Woe_UpdateSealNumbers_Response_ErrorCode

The errocode of the error. -1 if case of no error

### LoadingInfo_Woe_UpdateSealNumbers_Response_ErrorMessage

A human readable representation of the error, if any

*CompartmentOrderEntry info Request*

### LoadingInfo_Coe_Info_Level2To3

Sync tag PLC --> ALINE

### LoadingInfo_Coe_Info_Level3To2

Sync tag ALINE --> PLC

### LoadingInfo_Coe_Info_Request_Token

Tokennumber of the driver

### LoadingInfo_Coe_Info_Request_WoeId

Id of the workorderEntry the COE belongs to

### LoadingInfo_Coe_Info_Request_CoeId

Id of the compartment order entry to get info for. If none set (-1) next COE is automatically selected for the given woe

### LoadingInfo_Coe_Info_Request_LoadingSpot

The name of the loadingspot (should match the equipment name)

*CompartmentOrderEntry info response*

### LoadingInfo_Coe_Info_Response_CoeId

The id (Aline internal) of the compartmentorderentry

### LoadingInfo_Coe_Info_Response_State

State of the compartment order entry. Open = 1, InProgress = 2, Completed = 3, Aborted = 4

### LoadingInfo_Coe_Info_Response_Product

Name of the product to be loaded

### LoadingInfo_Coe_Info_Response_CompartmentNr

The number of the compartment of the truck to be loaded

### LoadingInfo_Coe_Info_Response_HandlingType

The handling type (Loading/unloading) Undefined=0, TankToTruck=1, TankToShip=2, TankToBarge=3, TankToRTC=4,  TruckToTank=5, ShipToTank=6, BargeToTank=7, RTCToTank=8, TruckToTruck=9, ShipToShip=10, BargeToBarge=11, ShipToBarge=12, BargeToShip=13, TruckToRTC=14, RTCToTruck=15, RTCToRTC=16, TankToTank=17, Circulation=18, Tracing=19, PreBlending=20

### LoadingInfo_Coe_Info_Response_ContainerNr

Identification of the container

### LoadingInfo_Coe_Info_Response_ContainerVolume

Capacity of the compartment

### LoadingInfo_Coe_Info_Response_PlannedMassKg

The planned amount in KgAir

### LoadingInfo_Coe_Info_Response_PlannedVolumeL

The planned amount in L GSV

### LoadingInfo_Coe_Info_Response_ErrorCode

The number of the error. -1 in case of no error

### LoadingInfo_Coe_Info_Response_ErrorMessage

A human readible representation of the error message

### LoadingInfo_Coe_Info_Response_Trans_Source_01

The name of the source of the first transfer (tank name for loading, loadingspot name on unloading)

### LoadingInfo_Coe_Info_Response_Trans_Destination_01

The name of the destination of the first transfer (loadingspot name on loading, tankname on unloading)

### LoadingInfo_Coe_Info_Response_Trans_Product_01

Name of the product to transfer

### LoadingInfo_Coe_Info_Response_Trans_Ratio_01

The ratio of the transfer compared to the compartment planned qty. (always one, until multiple transfers are implemented)

*CompartmentOrderEntry start request*

### LoadingInfo_Coe_RequestStart_Level2To3

Sync tag PLC -> ALINE

### LoadingInfo_Coe_RequestStart_Level3To2

Sync tag ALINE -> PLC

### LoadingInfo_Coe_RequestStart_Request_CoeId

Id (Aline internal) of the compartment order entry to start

### LoadingInfo_Coe_RequestStart_Request_LoadingSpot

The name of the loadingspot where the coe will be executed

*CompartmentOrderEntry start response*

### LoadingInfo_Coe_RequestStart_Response_ErrorCode

The error code after execution. -1 in case of no error

### LoadingInfo_Coe_RequestStart_Response_ErrorMessage

A human readable representation of the error message

*CompartmentOrderEntry info update*

### LoadingInfo_Coe_Update_Level2To3

Sync tag PLC -> ALINE

### LoadingInfo_Coe_Update_Level3To2

Sync tag ALINE --> PLC

### LoadingInfo_Coe_Update_Request_CoeId

The ID (Aline internal) of the COE to be updated

### LoadingInfo_Coe_Update_Request_End

Boolean indicating if the COE is completed (ended) or not

### LoadingInfo_Coe_Update_Request_Transfer_Source_01

The name of source of the first transfer. (tank name for loading, loadingspot name on unloading)

### LoadingInfo_Coe_Update_Request_Transfer_Destination_01

The name of the destination of the first transfer. (tank name for unloading, loadingspot name on loading)

### LoadingInfo_Coe_Update_Request_Transfer_WeightKg_01

The weight transferred for the first transfer in KgAir

### LoadingInfo_Coe_Update_Request_Transfer_VolumeL_01

The Volume transferred for the first transfer in L GSV

*CompartmentOrderEntry info update response*

### LoadingInfo_Coe_Update_Response_WoeId

The Id (Aline internal) of the workorderentry

### LoadingInfo_Coe_Update_Response_WoeState

The current state of the workorderentry. Open = 1, ReadyForTransfer = 2, InProgress = 3, Completed = 4, Validated = 5,  Closed = 6, Aborted = 7, Deleted = 8

### LoadingInfo_Coe_Update_Response_ErrorCode

The error code after the update. -1 in case of no error

### LoadingInfo_Coe_Update_Response_ErrorMessage

Human readable representation of the error.

*CompartmentOrderEntry Quality Update Request*

### LoadingInfo_Coe_UpdateQuality_Level2To3

Sync tag PLC --> ALINE

### LoadingInfo_Coe_UpdateQuality_Level3To2

Sync tag ALINE --> PLC

### LoadingInfo_Coe_UpdateQuality_Request_CoeId

Id (Aline internal) of the compartmentorderEntry of which the quality parameters should be updated

### LoadingInfo_Coe_UpdateQuality_Request_QualityParameterType

The name of the type of quality parameter. All names are valid like they are configured in the DB (eg. TEMPERATURE, DENSITY,...)

### LoadingInfo_Coe_UpdateQuality_Request_QualityValue

The value of the qaulityparameter. (decimal value)

*CompartmentOrderEntry update quality result*

### LoadingInfo_Coe_UpdateQuality_Response_ErrorCode

error code of the error. -1 in case of no error

### LoadingInfo_Coe_UpdateQuality_Response_ErrorMessage

Human readable representation of the error message


## error Codes

1001: Unable to reset tags

1002: Failure reading tags

1003: Failure writing tags


2001: No data found

2002: Invalid data found

2003: Unable to register start transfer

2004: Unable to update transfer

2005: Unable to register seal numbers

2010: Token not found

2011: Token operator not found

2012: Workorder not found

2013: Incorrect loading spot

2014: Workorder not found

2015: Data found but wrong loading spot

2016: Quality parameter unknown