### MassAir

This unit represents a Mass of a certain amount of substance in air.

#### Base unit

The internal value of MassAir is Kg.

#### Units

- Kg *(default)* - 1 * basevalue
```
MassAir.Kilogram
UnitOfMeasureTypes.KgAir
```
- g - 0.001 * basevalue
```
MassAir.Gram
UnitOfMeasureTypes.gramAir
```
- t - 1000 * basevalue
```
MassAir.Tonne
UnitOfMeasureTypes.tAir
```
- lt - 984.206 * basevalue
```
MassAir.LongTon
UnitOfMeasureTypes.ltAir
```

#### Additional methods

###### ConvertTo

Transform MassVac to MassAir
```
MassAir air = MassAir.Transform(vac);
```

#### Additional operators

###### MassAir / VolumeGOV

Dividing MassAir with a unit VolumeGOV results in a DensityAir

```
DensityAir dens = mass / volume
```

###### MassAir / VolumeGSV

Dividing MassAir with a unit VolumeGSV results in a DensityAirAtRef

```
DensityAirAtRef dens = mass / volume
```

###### MassAir / DensityAirAtRef

Dividing MassAir with a unit DensityAirAtRef results in a VolumeGSV

```
VolumeGSV volume = mass / dens
```
