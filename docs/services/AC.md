[Home](home)

# Access Control Service #
> Rev1: 2017-01-24

**Functions**

- Handle WorkOrderLocationChange requests
- Publish location changes of workorders
- Monitor and handle requests of AccessControlDevices
- Monitor queues and set calloff's


**Site specific parameters**

*CallOffUpdateInterval* (integer) 
The number of seconds to wait between two updates of the calloffs

*LocationTimeoutUpdateInterval* (integer)
The number of seconds to wait between timeout checks of locations

*publishTopicWorkOrderLocationChangeMsg* (string)
The rabbitMq topic to publish location change messages to

*subscribeTopicWorkOrderLocationChangeRequestMsg* (string)
The rabbitMq topic where location change request will be published to from other components

*badgereadersPrefix* (string)
The first part of the topic where to publish messages for accesscontrol devices to. (dispensers will also listen to dispense messages there)
The prefix will be appended with the device name to create a device specific topic.

*dispenseReadTimeout* (integer)
The amount of ms we want to wait until the dispensed tag is read. After the timeout the dispense is failed.


## Locations ##

The service handles all WorkOrder change request and also relocates workorders after a calloff, timeout on a location, valid booking on a accesscontrol device, or when a locationchangerequest message has been received from another service.

**ChangeLocationRequest**

Another service can request a location change for a workorder by sending a `WorkOrderLocationChangeRequestMsg` to the topic set in the *subscribeTopicWorkOrderLocationChangeRequestMsg* parameter.
The service will execute the change and move the location to the new location. When succeeded, a message will be published (see next chapter)
On failure, a warning message will be logged in the logfile

**WorkOrder location changed**

When a workorder has changed location for whatever reason, a `WorkOrderLocationChangeRegisteredMsg` will be published on the topic defined in parameter *publishTopicWorkOrderLocationChangeMsg*. This contains both the old and new location.

## Automatic location change ##

The AccessControl service monitors all locations and executes some authomatich tasks, if configured, on them.

**Call-Off**

At a certain interval (configured in the *CallOffUpdateInterval* parameter) the service will check all call-off's and see if a new order can be called off. If so, it moves the workorder to the `CalledOff` location defined in the calloff configuration.
A `WorkOrderLocationChangeRegisteredMsg` in case of a calloff.

**Time-out**

Each location in Aline has a timeout parameter. At each interval *LocationTimeoutUpdateInterval* the service checks all locations with an interval set and moves orders who are longer than the timeout in certain location to the defined timeoutlocation.
A `WorkOrderLocationChangeRegisteredMsg` is published in case of a timeout.


## Access Control ##

The access control handles all AccessControl Devices. These can be keypads, badge readers, barcode scanners, etc...
At startup all entries in the *AccessControlDevice* table are loaded and subscribed to. The name of the ACD should be the name of the FU in the OPCUAServer.


**Identification Validation**

When a identification is received for a device, following checks are made, in that order.

*Is the device enabled*

An ACD is an Equipment and thereby can be set to the enabled or disabled state. When the device is disabled, the identification is rejected.

*Identification is known in the system*

When the identification is not found in the database, the identification is rejected. In case of a swallower, the badge is ejected.

*Identification is locked*

A locked identification will be rejected, in case of a swallower, ejected.

*registerOnlocation is set*

The ACD has a parameter *RegisterOnLocation* which holds the id of a *location*. When set, the workorder this identification is attached to (if any) will be moved to that location instantly.

*Workorder states*

The ACD's parameter *ValidStates* defined the states a workorder should be in to be allowed access to this device. If the workorder is not in one of the valid states, identification will be rejected, in case of a swallower, ejected.
if no workorder is found, access is rejected. In case of swallower, badge is ejected.
In case there is an attached person and it is not a driver, this check is skipped.


*End weighing*

The *ShouldHaveEndWeighing* boolean parameter defines if the workorder should have an endweighing to pass this device. 
If no exitweighing is present, identification is rejected. In case of a swallower, the badge is ejected.
if no workorder is found, access is rejected. In case of swallower, badge is ejected.
In case there is an attached person and it is not a driver, this check is skipped.


*Check access*

A check for a valid access rule is done for the identification and the ACD for the current timestamp.
If no valid rule can be found, identification is rejected. In case of a swallower, badge is ejected.

*!!! At this point the identification is accepted!!!*
A `BadgeRegisteredMsg` will be published.

**Rejected Identification**

In case a identification gets rejected, the reject command is send to the ACD. Mostly this will result in a long beep and a red signaling light.
Meanwhile a `BadgeRejectedMsg` is published on the messagebus.

**Eject Identification**

In case the ACD is a swallower, the token can be ejected. The token wil pop back out of the device.
Also a `BedgeEjectedMsg` will be published on the bus.

**Swallow Identification**

In case of a swallower, tokens can get swallowed.
When swallowing the identification is detached from the workorder (if apliccable) and from the person (if appliccable)
If the person is a visitor, the visit will be ended.
When this happends, a `BadgeSwallowedMsg` is published on the messagebus.


**On an accepted identification**

Once an identification is accepted by the accesscontrol, several other things happen. (in this order)

*Close workorder*

If any workorder is present and the *CloseWorkorder* flag is set on the ACD, the workorder is closed.
The closing of a workorder might fail, in that case a `BadgeAutoCloseWorkOrderFailed` message is published on the bus.
On failure we also stop processing any further actions.

*Swallow Token*

This action is only valid in case of a swallower.

When the identification has the flag AllowSwallow and in case it is a visitor and the visit is ending, the token will be swallowed.
Any other case, the token is ejected.

*trigger barrier*

When a barrier is defined in the *IsTriggerForBarrier* field of the ACD, the barrier is triggered.
Whilest the barrier is triggered, the workorder (if connected) will be moved to the *transitionToLocation* of the barrier (if set).

## Dispenser ##

If the device is of type Swallower, the devices listens for `DispenseBadgeRequestMsg` messages on the bus.
This message contains a correlationId.
When such a message is received a badge is dispensed.
After dispense the reader waits for the dispensed token to be read. If read a `BadgeDispensdMsg` message is published with the token id.
If no badge is read within the timeout a `DispenseFailedMsg` Message (Failed to dispense) is published.
When the dispenser failed to trigger the dispense also a `DispenseFailedMsg` message (Tag read after dispense timeout) is published.


## Good To know's ##

- The access control devices are fetched and initialized on startup. Any changes in the configuration of those will require a restart of the service.