# UnitProvider

The UnitProvider does what you expect... Provide units ;)

On startup, it loads the default quantities in Aline and has the possibility to add other (unknown by the product) quantities using reflection.

You are able to use the quantities directly if you have a reference to them. If not, you can use the UnitProvider to help you.
Even if you do, the UnitProvider can help your parse, format and convert values.

The UnitProvider holds a culture. (the local culture by default) but you can change it. You can also have multiple UnitProviders with different units/cultures

Even if you do not use the UnitProvider explicitly, it is used. The Quantity objects use the UnitProvider to format their strings.

## Default UnitProvider

The UnitProvider class has the static property **Default**. Here you can access the default UnitProvider from anywhere.
The default UnitProvider has the default units from Aline already loaded. Same for the culture.

## Load your own units

You can load your own units by creating an instance of the UnitProvider and initialize your own assembly:

```c
var up = new UnitProvider(yourAssembly);
```

Or you can add them to an existing instance:

```c
UnitProvider.Default.RegisterUnits(yourAssembly);
```

Or you can add them by type:

```
UnitProvider.Default.RegisterUnits(myType);
```

Or load your Convertor type:

```
UnitProvider.Default.RegisterConversions(Type type);
```

### Usefull methods of the UnitProvider

#### GetTypeForUom()

```
Type GetTypeForUom(UnitOfMeasureTypes Uom)
```

Returns the type for the given Unit of measure. The result you can match with the different quantity types.

#### ParseUom()

The parse uom method convert a decimal and a UnitOfMeasureTypes item to an Quantity object.
*This only works for Aline core quantities*

```
IQuantity result = UnitProvider.Default.ParseUom(myDecimal, myUnitOfMeasureTypes);
```

For example, if you have a value in litres it will go as followed

```
IQuantity result = UnitProvider.Default.ParseUom(300m, UnitOfMeasureTypes.LGOV);
Console.WriteLine(result.ToString());
Output> 0.3 m^3
```

result will be of type VolumeGOV

#### Getconvertors

The GetConvertors method takes two parameters the source type and the destination type.
The method will return a list of *ConversionDefinition*. The ConversionDefinition contains the convertor, the source and destination type and a list of needed parameters with their description.

If no convertors are known for the given source and destination type, an empty list will be returned.

#### GetSymbolForUOM()

Returns the symbol string for the given UnitOfMeasureTypes

#### GetUnitForUom\<T\>()

Gets the unit for the given Quantity.

```
VolumeGOV.Litre;
```

Is the same as

```
UnitProvider.Default.GetUnitForUom<VolumeGOV>(UnitOfMeasureTypes.LGOV);
```

#### GetUomForType()

Gets the UnitOfMeasureTypes of the *default* unit of a quantity.

#### GetAllUomsForType(IQuantity qty)

Gets *all* the UnitOfMeasureTypes of a quantity

```
UnitProvider.Default.GetAllUomsForType(MassAir.Default);
```

will return

```
{
    UnitOfMeasureTypes.KgAir,
    UnitOfMeasureTypes.gramAir,
    UnitOfMeasureTypes.tAir,
    UnitOfMeasureTypes.ltAir
}
```